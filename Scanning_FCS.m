function varargout = Scanning_FCS(varargin)
% SCANNING_FCS MATLAB code for Scanning_FCS.fig
%      SCANNING_FCS, by itself, creates a new SCANNING_FCS or raises the existing
%      singleton*.
%
%      H = SCANNING_FCS returns the handle to a new SCANNING_FCS or the handle to
%      the existing singleton*.
%
%      SCANNING_FCS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SCANNING_FCS.M with the given input arguments.
%
%      SCANNING_FCS('Property','Value',...) creates a new SCANNING_FCS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Scanning_FCS_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Scanning_FCS_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE'startbtn2 Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Scanning_FCS

% Last Modified by GUIDE v2.5 12-Dec-2019 16:57:34

% Begin initialization code - DO NOT EDIT;
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Scanning_FCS_OpeningFcn, ...
                   'gui_OutputFcn',  @Scanning_FCS_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% functions for saving tiff images.
tiff_path = '.\saveastiff_4.4\';
if (exist(tiff_path,'dir') == 7)
    % 7 � name is a folder.
    % Check if bf_path is a folder. https://www.mathworks.com/help/matlab/ref/exist.html?s_tid=doc_ta
    addpath(tiff_path);
end

% --- Executes just before Scanning_FCS is made visible.
function Scanning_FCS_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Scanning_FCS (see VARARGIN)

% Choose default command line output for Scanning_FCS
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Scanning_FCS wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Scanning_FCS_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT); 
% hObject    handle to figure; 
% eventdata  reserved - to be defined in a future version of MATLAB; 
% handles    structure with handles and user data (see GUIDATA); 

% Get default command line output from handles structure; 
varargout{1} = handles.output;


% Line dwell time for FCS
function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
% Line dwell time for FCS
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Line length / radius for FCS
function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double

% Line length / radius for FCS
% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Pixel number for FCS
function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double

% --- Executes during object creation, after setting all properties.
% Pixel number for FCS
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in scan_mode2.
function scan_mode2_Callback(hObject, eventdata, handles)
% hObject    handle to scan_mode2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of scan_mode2
set(handles.edit1, 'string','1'); 

% --- Executes during object creation, after setting all properties.
% Pixel dwell time for scanning
function PixTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PixTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Pixel dwell time for scanning
function PixTime_Callback(hObject, eventdata, handles)
% hObject    handle to PixTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PixTime as text
%        str2double(get(hObject,'String')) returns contents of PixTime as a double


% --- Executes during object creation, after setting all properties.
% pixel number for confocal scanning.
function XPixNum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to XPixNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function XPixNum_Callback(hObject, eventdata, handles)
% hObject    handle to XPixNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of XPixNum as text
%        str2double(get(hObject,'String')) returns contents of XPixNum as a double

% --- Executes during object creation, after setting all properties.
% scan area for confocal

function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes on button press in StartBtn.
% The start button for confocal scanning.

function StartBtn_Callback(hObject, eventdata, handles)
    % hObject    handle to StartBtn (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    set(handles.StopBtn, 'userdata', 0);
    
    %%===================shutters and trigger setting=======================
    s = daq.createSession('ni'); %Create a Session;
    addDigitalChannel(s,'Dev2','Port0/Line1:3','OutputOnly'); % 3 for Confocal 640 nm, 2 for Tisa, 1 for 473 nm.
    addDigitalChannel(s,'Dev2','Port0/Line5','OutputOnly'); % 5 is for 561 and 473 nm shutters.
    addDigitalChannel(s,'Dev2','Port0/Line7','OutputOnly'); % Start trigger of bh board
    shutter473_value = get(handles.Shutter473, 'Value');
    shutter561_value = get(handles.Shutter561, 'Value');
    shutter640_value = get(handles.Shutter640, 'Value');
    shuttertisa_value = get(handles.TiSaShutter, 'Value');
    StartTrigger = get(handles.StartBH_1, 'Value'); % Start trigger for bh card.
    outputSingleScan(s,[shutter473_value, shuttertisa_value, shutter640_value, shutter561_value, StartTrigger]);
    stop(s);  release(s);
    %%========= end of shutter settings ========
    
    Axis_1_list = get(handles.ScanAxis_1,'String'); % scanning axes.
    Axis_1_value = get(handles.ScanAxis_1,'Value'); % scanning axes.
    Axis_1 = char(Axis_1_list(Axis_1_value));
    
    Axis_2_list = get(handles.ScanAxis_2,'String'); % scanning axes.
    Axis_2_value = get(handles.ScanAxis_2,'Value'); % scanning axes.
    Axis_2 = char(Axis_2_list(Axis_2_value));
    
    Axis_3_list = get(handles.ScanAxis_3,'String'); % scanning axes.
    Axis_3_value = get(handles.ScanAxis_3,'Value'); % scanning axes.
    Axis_3 = char(Axis_3_list(Axis_3_value));
    
    Pixel_dwelltime = get(handles.PixTime, 'string');
    Pixel_dwelltime = str2double(Pixel_dwelltime);
    Pixel_dwelltime = Pixel_dwelltime*1e-6; %Scanning period; Unit: microsecond; convert to second.
    
    Num_Pixel_X = str2double(get(handles.XPixNum, 'string'));
    Num_Pixel_Y = str2double(get(handles.YPixNum, 'string'));
    Num_Pixel_Z = str2double(get(handles.ZPixNum, 'string'));

    XLength = str2double(get(handles.XLength, 'string')); % scan area.
    YLength = str2double(get(handles.YLength, 'string')); % scan area.
    ZLength = str2double(get(handles.ZLength, 'string')); % scan area.

    % Scan_diameter_axial = str2double(get(handles.edit7, 'string')); % axial scan range (um)
    
    % Num_Pixel_axial = str2double(get(handles.edit9,'string'));
%     handles.Num_Pixel_axial = Num_Pixel_axial;
%     guidata(hObject, handles);
    
    Num_line_aver = str2double(get(handles.NumAveLine,'string')); % Number of lines to average
    Line_aver = get(handles.AveLine, 'value');  %0 for averaging, and 1 for not doing averaging.
    
    if Line_aver == 0
        Num_line_aver = 1;
    end 
    
    switch Axis_1
        case 'X Axis'
            Model_1 = 1;
        case 'Y Axis'
            Model_1 = 2;
        case 'Z Axis'
            Model_1 = 3;
        case 'None'
            Model_1 = 4;
    end
    
    switch Axis_2
        case 'X Axis'
            Model_2 = 1;
        case 'Y Axis'
            Model_2 = 2;
        case 'Z Axis'
            Model_2 = 3;
        case 'None'
            Model_2 = 4;
    end
    
    switch Axis_3
        case 'X Axis'
            Model_3 = 1;
        case 'Y Axis'
            Model_3 = 2;
        case 'Z Axis'
            Model_3 = 3;
        case 'None'
            Model_3 = 4;
    end
    
    if(Model_1 == 1 && Model_2 == 2 && Model_3 == 4)
        Image_model = 1; % XY
    elseif(Model_1 == 1 && Model_2 == 3 && Model_3 == 4)
        Image_model = 2; % XZ
    elseif(Model_1 == 2 && Model_2 == 3 && Model_3 == 4)
        Image_model = 3; % YZ
    elseif(Model_1 == 1 && Model_2 == 2 && Model_3 == 3)
        Image_model = 4; % XYZ
    elseif(Model_1 == 1 && Model_2 == 3 && Model_3 == 2)
        Image_model = 5; % XZY
    elseif(Model_1 == 2 && Model_2 == 3 && Model_3 == 1)
        Image_model = 6; % YZX
    end
    
    handles.Image_model = Image_model;
    guidata(hObject, handles);
   
    if(Image_model == 1) % XY 2D scan
          
        % devices = daq.getDevices; %discover a device
        % daq.getDevices; %discover a device; Not really needed.
        % devices(1), 
        % Create a Session: 
        s = daq.createSession('ni'); %Create a Session;
        handles.s = s; % Transfer s to the handle.
        guidata(hObject, handles);
        addAnalogOutputChannel(s,'Dev2',0,'Voltage'); 
        addAnalogOutputChannel(s,'Dev2',1,'Voltage'); 
        % addAnalogOutputChannel(startbtn2,'Dev2',2,'Voltage');
        % Only 2D scanning, so this line is not necessary.
        % addAnalogOutputChannel(startbtn2,'Dev2',3,'Voltage'); 
        % What is this line doing?
        % This is frame trigger, which is not needed for 2D imaging.
        % The NI board PCI6259 supports in total 4 AO ports.
        
        % ch = addCounterOutputChannel(startbtn2,'Dev2',0, 'PulseGeneration'); % Trigger output; line trigger.
        % Physically it is on the BNC2110 board.
        % Counter1 = addCounterInputChannel(startbtn2,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        % The variable "Counter1" is not used. Change it to the following
        % code.
        addDigitalChannel(s,'Dev2','Port0/Line8','OutputOnly'); 
        % Line trigger, Port0/Line8 is on the NI extension board.
        
        addCounterInputChannel(s,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        addCounterInputChannel(s,'Dev2','ctr0','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
%         addDigitalChannel(startbtn2,'Dev2','Port0/Line8','OutputOnly'); 
%         % Line trigger, Port0/Line8 is on the NI extension board.
        
        
        %Set the Session Rate; 
        Num_Line = Num_Pixel_Y;  % Line number; 
        
        Line_dwlltime = Pixel_dwelltime * Num_Pixel_X; %Scanning period; Unit: s; 
        Frame_dwelltime = Pixel_dwelltime*(Num_Pixel_X*Num_Line)*Num_line_aver; %Unit: s; 
        Num_Total_pixel = Num_Pixel_X*Num_Line*Num_line_aver; %Pixel number; 
        
        s.Rate = 1/Pixel_dwelltime; %Scanning rate; 
        t = linspace(0,Frame_dwelltime,Num_Total_pixel); %Time base; 
        t = t'; 
        
        %Rectangular scan: 
        %x scan % scan voltage to output.
        angle0 = 2*pi*t/Line_dwlltime;
        angle0 = angle(exp(1i*angle0+1i*pi))/pi; %Range: [-1,1]; 
        outputSignal1 = XLength * 0.01035 * angle0 - 0.275; %-0.2750; %x scan; 
        % The numbers were adjusted such that the imaged area is the same
        % as shown in Imspector.
        %y scan        
        angle0 = (floor(t/(Line_dwlltime * Num_line_aver)) - Num_Line/2)/(Num_Line/2); %Range: [-1,1]; 
        outputSignal2 = YLength*0.01035*angle0-0.343; %-0.3430; %y scan; D;     
        % the size of outputSignal1 and outputSignal2 is Num_Total_pixelx1.
        
        %z scan: z scan is not done.
        % z_scan = linspace(-Scan_diameter_axial/2,Scan_diameter_axial/2,Num_Pixel_axial); %Axial scanning range;
        % z1 = z_scan(1);
        % outputSignal3=z1*0.0975*ones(size(outputSignal1))+5.484; %Control aixal position;4.8(in the beginning)  4.8975
        % There is no outputSignal3.
        
        % Frame-trigger: 
        % outputSignal4 = ones(size(outputSignal1)); outputSignal4 = -1*outputSignal4; outputSignal4(1) = 4; %Frame trigger;
        % It does not look doing any serious thing.
        
        %Queue the data:         
%         queueOutputData(startbtn2,[outputSignal1(1:1e4) outputSignal2(1:1e4) outputSignal4(1:1e4)]); %Analogy output; 
        % Device.IsContinuous = true;
        % startbtn2.IsContinuous = true; % It should be like this.
        % The startForeground method cannot be called with IsContinuous set to true.  Use startBackground.
        
        outputSignal3 = zeros(Num_Pixel_Y, Num_Pixel_X * Num_line_aver);
        outputSignal3(:,1) = ones(Num_Pixel_Y, 1); % Line trigger. High level on the first pixel.
        outputSignal3 = reshape(outputSignal3', [Num_Total_pixel, 1]);
        
        % Status message
        set(handles.text10,'string','Scanning 2D XY image...'); 
        set(handles.text10,'BackgroundColor','r'); 
        
        %[captured_data,time] = startbtn2.startForeground(); 
        disp('start at this moment'); 
%         Data =startForeground(startbtn2); %Start the Session in Foreground;         
%         Data=diff(Data,1,1); Data=[Data;0]; %Data;         
%         Data=reshape(Data,[sqrt(length(Data)),sqrt(length(Data))])'; 
%         %Data=reshape(Data,[Num_Pixel*Num_line_aver,Num_Line])'; 
%         gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Data); colormap('hot'); colorbar; axis off;
%         
%         %Time series of the interferograms:      
%         %z_scan=linspace(-Scan_diameter_axial/2,Scan_diameter_axial/2,Num_Pixel_axial); %Axial scanning range; 
%         
%         %for ii=1:Num_Pixel_axial,           
%         %stop(startbtn2); release(startbtn2);              
%         %z1=z_scan(ii);
%         %outputSignal3=z1*0.0975*ones(size(outputSignal1))+5.3984; %Control aixal position;4.8(in the beginning) 0,384  
%         stop(startbtn2);  release(startbtn2);
          % queueOutputData(startbtn2,[outputSignal1 outputSignal2 outputSignal4]); %Analogy output;  
        queueOutputData(s,[outputSignal1 outputSignal2 outputSignal3]); % Signal output;
        % When generating output signals, you must queue data before you call startForeground or startBackground.

        % Clocked Counter Output(to synchronize line trigger): 
        % It generates a pulse train with a defined frequency during the
        % scanning. So it ends up with more line triggers than expected
        % because of the extra time during scanner flying back! (not sure
        % of this)
        % ch = startbtn2.Channels(4); % addCounterOutputChannel, channel for line
        % trigger output. ch is defined before.
        
        % ch.Frequency = 1/(Line_dwlltime*Num_line_aver); % Trigger output frequency;
        
        % Here I should add the scaner fly back time. How much is it?
        % A line trigger is generated after the lines to average are
        % finished.
        % ch.InitialDelay = Line_dwlltime/0.95; % Innitial delay; % What is this doing? Why 1.98?
        % The delay factor can be adjusted according to the pixel number
        % and px dwell time.
        % It is better not to use any delay. 
        
        % ch.DutyCycle = 1e-4;  %Duty cycle; 10ns Pulse width;
        
        % Line trigger pulse width = Line_dwlltime*Num_line_aver *
        % ch.DutyCycle
        
        Data = startForeground(s); %Start the Session in Foreground;
        % startBackground(startbtn2) does not have output arguments.
        
        stop(s);  release(s);
                %%=================== close all shutters =======================
        s = daq.createSession('ni'); %Create a Session;
        addDigitalChannel(s,'Dev2','Port0/Line1:3','OutputOnly'); % 3 for Confocal 640 nm, 2 for Tisa, 1 for 473.
        addDigitalChannel(s,'Dev2','Port0/Line5','OutputOnly'); % 5 is for 561 and 473 nm shutters.
        addDigitalChannel(s,'Dev2','Port0/Line7','OutputOnly'); % make the bh trigger signal low
        outputSingleScan(s,[0 0 0 0 0]);
        stop(s);  release(s);
        %%========= end of shutter settings ========
        %end
        
        % Show the aquired image -- channel red
        Data1 = diff(Data(:,1),1,1);
        Data1 = [Data1; 0]; %Data; 
        Image1 = reshape(Data1,[Num_Pixel_X*Num_line_aver, Num_Line])'; %Interferogram 1;        
        gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Image1); 
        % caxis([0,50]); % Force the maximum intensity to be 50.
        colormap('hot'); colorbar; axis off;
        axis image
        
        handles.image1 = Image1; % Transfer Image1 to the handle.
        guidata(hObject, handles); % This is for saving the image.
        
        % Show the aquired image -- channel blue
        Data2 = diff(Data(:,2),1,1); Data2 = [Data2; 0]; %Data; 
        Image2 = reshape(Data2,[Num_Pixel_X*Num_line_aver, Num_Line])'; %Interferogram 1;        
        gcf; set(gcf,'currentaxes',handles.axes2); imagesc(Image2); 
        % caxis([0,50]); % Force the maximum intensity to be 50.
        colormap('hot'); colorbar; axis off;
        axis image
        
        handles.image2 = Image2; % Transfer Image2 to the handle.
        guidata(hObject, handles);

        %state declaration
        set(handles.text10,'string','Scanning finished!'); 
        set(handles.text10,'BackgroundColor','y'); 
        disp('Finished!');
        
        %==================== Save the recorded images automatically.
        autosave_value = get(handles.AutoSave_1, 'Value');
        if(autosave_value)
            folder = get(handles.Path_1, 'String');
            name1 = 'scan_1_image_1.tif';
            name2 = 'scan_1_image_2.tif';
            full_path1 = strcat(folder, name1);
            full_path2 = strcat(folder, name2);
            options.message = false; % Show the message in the message box, not in the com window.
            saveastiff(uint16(Image1),full_path1,options);
            saveastiff(uint16(Image2),full_path2,options);
        end
        %----============Autosave finishes ---
    
    elseif(Image_model == 2) % XZ 2D scan
          
        % devices = daq.getDevices; %discover a device
        % daq.getDevices; %discover a device; Not really needed.
        % devices(1), 
        % Create a Session: 
        s = daq.createSession('ni'); %Create a Session;
        handles.s = s; % Transfer s to the handle.
        guidata(hObject, handles);
        addAnalogOutputChannel(s,'Dev2',0,'Voltage'); % X axis
        addAnalogOutputChannel(s,'Dev2',2,'Voltage'); % Z axis
        % XZ scan
        addDigitalChannel(s,'Dev2','Port0/Line8','OutputOnly'); 
        % Line trigger, Port0/Line8 is on the NI extension board.
        
        % ch = addCounterOutputChannel(startbtn2,'Dev2',0, 'PulseGeneration'); % Trigger output; line trigger.
        % Physically it is on the BNC2110 board.
        % Counter1 = addCounterInputChannel(startbtn2,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        % The variable "Counter1" is not used. Change it to the following code.
        
        addCounterInputChannel(s,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        addCounterInputChannel(s,'Dev2','ctr0','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
%         addDigitalChannel(startbtn2,'Dev2','Port0/Line8','OutputOnly'); 
%         % Line trigger, Port0/Line8 is on the NI extension board.
        

        %Set the Session Rate; 
        Num_Line = Num_Pixel_Z;  % Line number; 
        
        Line_dwlltime = Pixel_dwelltime * Num_Pixel_X; % Scanning period; Unit: s; 
        Frame_dwelltime = Pixel_dwelltime*(Num_Pixel_X*Num_Line)*Num_line_aver; %Unit: s; 
        Num_Total_pixel = Num_Pixel_X*Num_Line*Num_line_aver; %Pixel number; 
        
        s.Rate = 1/Pixel_dwelltime; %Scanning rate; 
        t = linspace(0,Frame_dwelltime,Num_Total_pixel); %Time base; 
        t = t'; 
        
        %Rectangular scan: 
        %x scan % scan voltage to output.
        angle0 = 2*pi*t/Line_dwlltime;
        angle0 = angle(exp(1i*angle0+1i*pi))/pi; %Range: [-1,1]; 
        outputSignal1 = XLength * 0.01035 * angle0 - 0.275; %-0.2750; %x scan; 
        % The numbers were adjusted such that the imaged area is the same
        % as shown in Imspector.
        %y scan        
        angle0 = (floor(t/(Line_dwlltime * Num_line_aver)) - Num_Line/2)/(Num_Line/2); %Range: [-1,1]; 
        outputSignal2 = ZLength*0.0480*angle0 + 4.6159; % z scan
        % the size of outputSignal1 and outputSignal2 is Num_Total_pixelx1.
        
        outputSignal3 = zeros(Num_Pixel_Z, Num_Pixel_X*Num_line_aver);
        outputSignal3(:,1) = ones(Num_Pixel_Z,1); % Line trigger. High level on the first pixel.
        outputSignal3 = reshape(outputSignal3',[Num_Total_pixel, 1]);
        
        % Status message
        set(handles.text10,'string','Scanning 2D XZ image...'); 
        set(handles.text10,'BackgroundColor','r'); 
        
        %[captured_data,time] = startbtn2.startForeground(); 
        disp('start at this moment'); 
%         Data =startForeground(startbtn2); %Start the Session in Foreground;         
%         Data=diff(Data,1,1); Data=[Data;0]; %Data;         
%         Data=reshape(Data,[sqrt(length(Data)),sqrt(length(Data))])'; 
%         %Data=reshape(Data,[Num_Pixel*Num_line_aver,Num_Line])'; 
%         gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Data); colormap('hot'); colorbar; axis off;
%         
%         %Time series of the interferograms:      
%         %z_scan=linspace(-Scan_diameter_axial/2,Scan_diameter_axial/2,Num_Pixel_axial); %Axial scanning range; 
%         
%         %for ii=1:Num_Pixel_axial,           
%         %stop(startbtn2); release(startbtn2);              
%         %z1=z_scan(ii);
%         %outputSignal3=z1*0.0975*ones(size(outputSignal1))+5.3984; %Control aixal position;4.8(in the beginning) 0,384  
%         stop(startbtn2);  release(startbtn2);
          % queueOutputData(startbtn2,[outputSignal1 outputSignal2 outputSignal4]); %Analogy output;  
        queueOutputData(s,[outputSignal1 outputSignal2 outputSignal3]); % Signal output;

        % Clocked Counter Output(to synchronize line trigger): 
        % It generates a pulse train with a defined frequency during the
        % scanning. So it ends up with more line triggers than expected
        % because of the extra time during scanner flying back!
        % ch = startbtn2.Channels(4); % addCounterOutputChannel, channel for line
        % trigger output. ch is defined before.
        
        % ch.Frequency = 1/(Line_dwlltime*Num_line_aver); % Trigger output frequency;
        
        % Here I should add the scaner fly back time. How much is it?
        % A line trigger is generated after the lines to average are
        % finished.
        % ch.InitialDelay = Line_dwlltime/0.95; % Innitial delay; % What is this doing? Why 1.98?
        % The delay factor can be adjusted according to the pixel number
        % and px dwell time.
        % It is better not to use any delay. 
        
        % ch.DutyCycle = 1e-4;  %Duty cycle; 10ns Pulse width;
        
        % Line trigger pulse width = Line_dwlltime*Num_line_aver *
        % ch.DutyCycle
        
        Data = startForeground(s); %Start the Session in Foreground;
        
        stop(s);  release(s);
        
                %%=================== close all shutters =======================
        s = daq.createSession('ni'); %Create a Session;
        addDigitalChannel(s,'Dev2','Port0/Line1:3','OutputOnly'); % 3 for Confocal 640 nm, 2 for Tisa, 1 for 473
        addDigitalChannel(s,'Dev2','Port0/Line5','OutputOnly'); % 5 is for 561 and 473 nm shutters.
        addDigitalChannel(s,'Dev2','Port0/Line7','OutputOnly'); % Start trigger of bh board
        outputSingleScan(s,[0 0 0 0 0]);
        stop(s);  release(s);
        %%========= end of shutter settings ========
        %end
        
        
        % Show the aquired image -- channel red
        Data1 = diff(Data(:,1),1,1);
        Data1 = [Data1; 0]; %Data; 
        Image1 = reshape(Data1,[Num_Pixel_X*Num_line_aver, Num_Line])'; %Interferogram 1;        
        gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Image1); 
        % caxis([0,50]); % Force the maximum intensity to be 50.
        colormap('hot'); colorbar; axis off;
        axis image
        
        handles.image1 = Image1; % Transfer Image1 to the handle.
        guidata(hObject, handles); % This is for saving the image.
        
        % Show the aquired image -- channel blue
        Data2 = diff(Data(:,2),1,1); Data2 = [Data2; 0]; %Data; 
        Image2 = reshape(Data2,[Num_Pixel_X*Num_line_aver, Num_Line])'; %Interferogram 1;        
        gcf; set(gcf,'currentaxes',handles.axes2); imagesc(Image2); 
        % caxis([0,50]); % Force the maximum intensity to be 50.
        colormap('hot'); colorbar; axis off;
        axis image
        
        handles.image2 = Image2; % Transfer Image2 to the handle to save the image manually later.
        guidata(hObject, handles);


        %state declaration
        set(handles.text10,'string','Scanning finished!'); 
        set(handles.text10,'BackgroundColor','y'); 
        disp('Finished!');
        
        %==================== Save the recorded images automatically.
        autosave_value = get(handles.AutoSave_1, 'Value');
        if(autosave_value)
            folder = get(handles.Path_1, 'String');
            name1 = 'scan_1_image_1.tif';
            name2 = 'scan_1_image_2.tif';
            full_path1 = strcat(folder, name1);
            full_path2 = strcat(folder, name2);
            options.message = false; % Show the message in the message box, not in the com window.
            saveastiff(uint16(Image1),full_path1,options);
            saveastiff(uint16(Image2),full_path2,options);
        end
        %----============Autosave finishes ---
        
    elseif(Image_model == 3) % YZ 2D scan
          
        % devices = daq.getDevices; %discover a device
        % daq.getDevices; %discover a device; Not really needed.
        % devices(1), 
        % Create a Session: 
        s = daq.createSession('ni'); %Create a Session;
        handles.s = s; % Transfer s to the handle.
        guidata(hObject, handles);
        addAnalogOutputChannel(s,'Dev2',1,'Voltage'); 
        addAnalogOutputChannel(s,'Dev2',2,'Voltage'); 
        % YZ scan
        addDigitalChannel(s,'Dev2','Port0/Line8','OutputOnly'); 
%       % Line trigger
        
        % ch = addCounterOutputChannel(startbtn2,'Dev2',0, 'PulseGeneration'); % Trigger output; line trigger.
        % Physically it is on the BNC2110 board.
        % Counter1 = addCounterInputChannel(startbtn2,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        % The variable "Counter1" is not used. Change it to the following code.
        
        addCounterInputChannel(s,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        addCounterInputChannel(s,'Dev2','ctr0','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
%         addDigitalChannel(startbtn2,'Dev2','Port0/Line8','OutputOnly'); 
%         % Line trigger

        %Set the Session Rate; 
        Num_Line = Num_Pixel_Z;  % Line number; 
        
        Line_dwlltime = Pixel_dwelltime * Num_Pixel_Y; % Scanning period; Unit: s; 
        Frame_dwelltime = Pixel_dwelltime*(Num_Pixel_Y*Num_Line)*Num_line_aver; %Unit: s; 
        Num_Total_pixel = Num_Pixel_Y*Num_Line*Num_line_aver; %Pixel number; 
        
        s.Rate = 1/Pixel_dwelltime; %Scanning rate; 
        t = linspace(0,Frame_dwelltime,Num_Total_pixel); %Time base; 
        t = t'; 
        
        %Rectangular scan: 
        %x scan % scan voltage to output.
        angle0 = 2*pi*t/Line_dwlltime;
        angle0 = angle(exp(1i*angle0+1i*pi))/pi; %Range: [-1,1]; 
        outputSignal1 = YLength * 0.01035 * angle0 -0.343; 
        % The numbers were adjusted such that the imaged area is the same
        % as shown in Imspector.
        %y scan        
        angle0 = (floor(t/(Line_dwlltime * Num_line_aver)) - Num_Line/2)/(Num_Line/2); %Range: [-1,1]; 
        outputSignal2 = ZLength*0.0480*angle0 + 4.6159; % z scan
        % the size of outputSignal1 and outputSignal2 is Num_Total_pixelx1.
        
        outputSignal3 = zeros(Num_Pixel_Z, Num_Pixel_Y*Num_line_aver);
        outputSignal3(:,1) = ones(Num_Pixel_Z,1); % Line trigger. High level on the first pixel.
        outputSignal3 = reshape(outputSignal3',[Num_Total_pixel, 1]);
        
        % Status message
        set(handles.text10,'string','Scanning 2D YZ image...'); 
        set(handles.text10,'BackgroundColor','r'); 
        
        %[captured_data,time] = startbtn2.startForeground(); 
        disp('start at this moment'); 
%         Data =startForeground(startbtn2); %Start the Session in Foreground;         
%         Data=diff(Data,1,1); Data=[Data;0]; %Data;         
%         Data=reshape(Data,[sqrt(length(Data)),sqrt(length(Data))])'; 
%         %Data=reshape(Data,[Num_Pixel*Num_line_aver,Num_Line])'; 
%         gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Data); colormap('hot'); colorbar; axis off;
%         
%         %Time series of the interferograms:      
%         %z_scan=linspace(-Scan_diameter_axial/2,Scan_diameter_axial/2,Num_Pixel_axial); %Axial scanning range; 
%         
%         %for ii=1:Num_Pixel_axial,           
%         %stop(startbtn2); release(startbtn2);              
%         %z1=z_scan(ii);
%         %outputSignal3=z1*0.0975*ones(size(outputSignal1))+5.3984; %Control aixal position;4.8(in the beginning) 0,384  
%         stop(startbtn2);  release(startbtn2);
          % queueOutputData(startbtn2,[outputSignal1 outputSignal2 outputSignal4]); %Analogy output;  
        queueOutputData(s,[outputSignal1 outputSignal2 outputSignal3]); % Signal output;

        % Clocked Counter Output(to synchronize line trigger): 
        % It generates a pulse train with a defined frequency during the
        % scanning. So it ends up with more line triggers than expected
        % because of the extra time during scanner flying back!
        % ch = startbtn2.Channels(4); % addCounterOutputChannel, channel for line
        % trigger output. ch is defined before.
        
        % ch.Frequency = 1/(Line_dwlltime*Num_line_aver); % Trigger output frequency;
        
        % Here I should add the scaner fly back time. How much is it?
        % A line trigger is generated after the lines to average are
        % finished.
        % ch.InitialDelay = Line_dwlltime/0.95; % Innitial delay; % What is this doing? Why 1.98?
        % The delay factor can be adjusted according to the pixel number
        % and px dwell time.
        % It is better not to use any delay. 
        
        % ch.DutyCycle = 1e-4;  %Duty cycle; 10ns Pulse width;
        
        % Line trigger pulse width = Line_dwlltime*Num_line_aver *
        % ch.DutyCycle
        
        Data = startForeground(s); %Start the Session in Foreground;
        
        stop(s);  release(s);
        
                %%=================== close all shutters =======================
        s = daq.createSession('ni'); %Create a Session;
        addDigitalChannel(s,'Dev2','Port0/Line1:3','OutputOnly'); % 3 for Confocal 640 nm, 2 for Tisa, 1 for 473.
        addDigitalChannel(s,'Dev2','Port0/Line5','OutputOnly'); % 5 is for 561 and 473 nm shutters.
        addDigitalChannel(s,'Dev2','Port0/Line7','OutputOnly'); % Start trigger of bh board
        outputSingleScan(s,[0 0 0 0 0]);
        stop(s);  release(s);
        %%========= end of shutter settings ========
        
        % Show the aquired image -- channel red
        Data1 = diff(Data(:,1),1,1);
        Data1 = [Data1; 0]; %Data; 
        Image1 = reshape(Data1,[Num_Pixel_Y*Num_line_aver, Num_Line])'; %Interferogram 1;        
        gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Image1); 
        % caxis([0,50]); % Force the maximum intensity to be 50.
        colormap('hot'); colorbar; axis off;
        axis image
        
        handles.image1 = Image1; % Transfer Image1 to the handle.
        guidata(hObject, handles); % This is for saving the image.
        
        % Show the aquired image -- channel blue
        Data2 = diff(Data(:,2),1,1); Data2 = [Data2; 0]; %Data; 
        Image2 = reshape(Data2,[Num_Pixel_Y*Num_line_aver, Num_Line])'; %Interferogram 1;        
        gcf; set(gcf,'currentaxes',handles.axes2); imagesc(Image2); 
        % caxis([0,50]); % Force the maximum intensity to be 50.
        colormap('hot'); colorbar; axis off;
        axis image
        
        handles.image2 = Image2; % Transfer Image2 to the handle.
        guidata(hObject, handles);

        %state declaration
        set(handles.text10,'string','Scanning finished!'); 
        set(handles.text10,'BackgroundColor','y'); 
        disp('Finished!');
        
        %==================== Save the recorded images automatically.
        autosave_value = get(handles.AutoSave_1, 'Value');
        if(autosave_value)
            folder = get(handles.Path_1, 'String');
            name1 = 'scan_1_image_1.tif';
            name2 = 'scan_1_image_2.tif';
            full_path1 = strcat(folder, name1);
            full_path2 = strcat(folder, name2);
            options.message = false; % Show the message in the message box, not in the com window.
            saveastiff(uint16(Image1),full_path1,options);
            saveastiff(uint16(Image2),full_path2,options);
        end
        %----============Autosave finishes ---
        
        
    elseif(Image_model == 4) % XYZ 3D imaging
        
        zin = daq.createSession('ni'); % create a session to measure voltage.
        addAnalogInputChannel(zin, 'Dev2', 'ai0', 'Voltage'); % measure the voltage at the z piezo before the scanning starts
        init_z = inputSingleScan(zin); % measures the voltage.
        stop(zin);
        release(zin); % Stop and release the session.
        %For test:         
        % Num_Pixel=256;Pixel_dwelltime=30e-6;Num_line_aver=10;Scan_diameter=20;
        
        % Path_name='D:\Experiment\2017_09_29_tcspc\Phase_imaging\Cell4_130_130um\'; Do not know what it does.  
        % daq.getDevices; %discover a device; not really needed.
        % devices = daq.getDevices; %discover a device
        % devices(1), 
        %Create a Session: 
        s = daq.createSession('ni'); %Create a Session; 
        handles.s = s; % Transfer s to the handle.
        guidata(hObject, handles);      
 
        addAnalogOutputChannel(s,'Dev2',0,'Voltage'); 
        addAnalogOutputChannel(s,'Dev2',1,'Voltage'); 
        addAnalogOutputChannel(s,'Dev2',2,'Voltage'); % The channel that control the z position.
        % z axis
%         addAnalogOutputChannel(startbtn2,'Dev2',3,'Voltage');
        % For the frame triggers
        
        
%         ch = addCounterOutputChannel(startbtn2,'Dev2',0, 'PulseGeneration'); % Line trigger output;
        addDigitalChannel(s, 'Dev2', 'Port0/Line8', 'OutputOnly'); 
        % Line trigger, Port0/Line8 is on the NI extension board.
        % addDigitalChannel(startbtn2, 'Dev2', 'Port0/Line9', 'OutputOnly');
        % Frame trigger, Port0/Line9 is on the NI extension board.
        
        % Counter1 = addCounterInputChannel(startbtn2,'Dev2','ctr1','EdgeCount');
        % Orignal code
%         addCounterInputChannel(startbtn2,'Dev2','ctr1','EdgeCount');
        % Detector signal input.
        addCounterInputChannel(s,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        addCounterInputChannel(s,'Dev2','ctr0','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        
        %Set the Session Rate; 
        % Num_Pixel=Num_Pixel; %Pixel number; Unnecessary
        Num_Line = Num_Pixel_Y;  %Line number; 
        % Pixel_dwelltime=Pixel_dwelltime; %pixel dwell time; Unit: startbtn2;  Unnecessary
        
        Line_dwlltime = Pixel_dwelltime * Num_Pixel_X; %Scanning period; Unit: s; 
        Frame_dwelltime = Pixel_dwelltime*(Num_Pixel_X*Num_Line)*Num_line_aver; %Unit: s; 
        Num_Total_pixel = Num_Pixel_X*Num_Line*Num_line_aver; %Pixel number; 
        
        s.Rate = 1/Pixel_dwelltime; %Scanning rate; 
        t=linspace(0,Frame_dwelltime,Num_Total_pixel); %Time base; 
        t=t'; 
        
        %Rectangular scan: 
        %x scan
        % Scan_diameter = Scan_diameter; %Unit: um; Unnecessary
        angle0 = 2*pi*t/Line_dwlltime; angle0=angle(exp(1i*angle0+1i*pi))/pi; %Range: [-1,1]; 
        outputSignal1 = XLength*0.01035*angle0 - 0.275; %-0.2750; %x scan; 
        % outputSignal1 is a column vector. The size is the total number of
        % pixels in a 2D scan.
        %y scan        
        angle0 = (floor(t/(Line_dwlltime*Num_line_aver))-Num_Line/2)/(Num_Line/2); %Range: [-1,1]; 
        outputSignal2 = YLength*0.01035*angle0 - 0.343; %-0.3430; %y scan; D;         
        %z scan: 
        % z_scan = linspace(-Scan_diameter_axial/2,Scan_diameter_axial/2,Num_Pixel_axial); %Axial scanning range;
        % z1=z_scan(1);
        % outputSignal3 = z1*0.0975*ones(size(outputSignal1)) + 5.484; %Control aixal position;4.8(in the beginning)  4.8975   
        % This line is not defining the z scan range.
        
        outputSignal4 = zeros(Num_Pixel_Y, Num_Pixel_X * Num_line_aver);
        outputSignal4(:,1) = ones(Num_Pixel_Y, 1); % Line trigger. High level on the first pixel.
        outputSignal4 = reshape(outputSignal4', [Num_Total_pixel, 1]);
%         outputSignal4 = repmat(outputSignal4, Num_Pixel_Z, 1);
%         outputSignal4 = reshape(outputSignal4', [Num_Total_pixel*Num_Pixel_Z, 1]);
        % Line triggers on P0.8.
        
%         %Frame-trigger: (generated on AO3 port).
%         % outputSignal4 = ones(size(outputSignal3));
%         outputSignal5 = ones(size(outputSignal1));
%         outputSignal5 = -1*outputSignal5;
%         outputSignal5(1) = 4; %Frame trigger;

        % Frame trigger, generated on P0.9 (line0/port9, on the extension
        % board)
        % Frame triggers can be unavailable if the number of lines in each
        % frame is exactly the same.
%         outputSignal5 = zeros(Num_Total_pixel, 1);
%         outputSignal5(1) = 1; % High level on the first pixel.
        
        %Queue the data: 
        % queueOutputData(startbtn2,[outputSignal1(1:1e4) outputSignal2(1:1e4) outputSignal3(1:1e4) outputSignal4(1:1e4)]); %Analogy output; 
        % Device.IsContinuous = true;
        % startbtn2.IsContinuous = true; % It should be like this.
        % The startForeground method cannot be called with IsContinuous set to true.  Use startBackground.
        
        % Status message
        set(handles.text10,'string','Running'); 
        set(handles.text10,'BackgroundColor','r'); 
        
        %[captured_data,time] = startbtn2.startForeground(); 
        disp('start at this moment'); 
        
        % This part is a prescan.
%         Data = startForeground(startbtn2); %Start the Session in Foreground;         
%         Data = diff(Data,1,1);
%         Data = [Data;0]; %Data; % Add a zero after diff.         
%         Data = reshape(Data,[sqrt(length(Data)),sqrt(length(Data))])'; 
%         %Data=reshape(Data,[Num_Pixel*Num_line_aver,Num_Line])'; 
%         gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Data);
%         colormap('hot'); colorbar; axis off;
        
        %Time series of the interferograms:
       % Here starts the z scan.
        % z_scan = linspace(-Scan_diameter_axial/2, Scan_diameter_axial/2, Num_Pixel_axial); %Axial scanning range;
        z_scan = linspace(-ZLength/2, ZLength/2, Num_Pixel_Z); % Axial scanning range;
        
        Image1 = zeros(Num_Line, Num_Pixel_X * Num_line_aver, Num_Pixel_Z); % 3D image data.
        Image2 = zeros(Num_Line, Num_Pixel_X * Num_line_aver, Num_Pixel_Z);
        handles.image1 = zeros(Num_Line, Num_Pixel_X * Num_line_aver, Num_Pixel_Z); % 3D image data.
        guidata(hObject, handles); % This is for saving the image.
        handles.image2 = zeros(Num_Line, Num_Pixel_X * Num_line_aver, Num_Pixel_Z); % 3D image data.
        guidata(hObject, handles); % This is for saving the image.
        
        for ii=1:Num_Pixel_Z
            msgstr = sprintf('Scaning frame %5.0f out of %5.0f frames...', ii, Num_Pixel_Z); 
            set(handles.text10,'string',msgstr); 
            set(handles.text10,'BackgroundColor','r'); 
            
            stop(s); release(s);    % This part is needed.
            
            % drawnow
            if get(handles.StopBtn, 'userdata') % Stop the loop if the stop buttton is pressed.
                break;
            end
            
%             handles.startbtn2 = startbtn2; % Transfer startbtn2 to the handle.
%             guidata(hObject, handles);
            
            z1 = z_scan(ii);
            outputSignal3 = z1*0.096*ones(size(outputSignal1)) + 4.6159; %Control axial position;4.8(in the beginning) 0,384  
            % Adjust the the two parameters so that we are imaging the same planes as in Imspector. 
            % outputSignal3=z1*0.0975*ones(size(outputSignal1))+5.484; %Control aixal position;4.8(in the beginning)  4.8975 
            % at the beginning.
            
            % queueOutputData(startbtn2,[outputSignal1 outputSignal2 outputSignal3 outputSignal4 outputSignal5]);
            queueOutputData(s,[outputSignal1 outputSignal2 outputSignal3 outputSignal4]);
            % OutputsL outputSignal,2,3 are analogy signals of scanning,
            % outputSignal4 is digital output for line trigger and
            % outputSignal5 is digital output for frame trigger.
            % Frame triggers can be unavailable if the number of lines in each
            % frame is exactly the same.

            %Clocked Counter Output(to synchronize line trigger): 
            % ch = startbtn2.Channels(5); % Here it becomes channel 5. 
%             ch.Frequency = 1/(Line_dwlltime*Num_line_aver); %Trigger output frequency; 
%             % ch.InitialDelay = Line_dwlltime/0.95; %Innitial delay; 
%             ch.DutyCycle = 1e-4;  %Duty cycle; 10ns Pulse width; 

            Data = startForeground(s); %Start the Session in Foreground;
            stop(s);  release(s);
            
            
            % Show the aquired image -- channel red
            Data1 = diff(Data(:,1),1,1);
            Data1 = [Data1; 0]; %Data; 
            Image1(:,:,ii) = reshape(Data1,[Num_Pixel_X*Num_line_aver, Num_Line])'; %Interferogram 1;        
            gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Image1(:,:,ii)); 
            % caxis([0,50]); % Force the maximum intensity to be 50.
            colormap('hot'); colorbar; axis off;
            axis image

            handles.image1(:,:,ii) = Image1(:,:,ii); % Transfer Image1 to the handle.
            guidata(hObject, handles); % This is for saving the image.

            % Show the aquired image -- channel blue
            Data2 = diff(Data(:,2),1,1);
            Data2 = [Data2; 0]; %Data; 
            Image2(:,:,ii) = reshape(Data2,[Num_Pixel_X*Num_line_aver, Num_Line])'; %Interferogram 1;        
            gcf; set(gcf,'currentaxes',handles.axes2);
            imagesc(Image2(:,:,ii)); 
            % caxis([0,50]); % Force the maximum intensity to be 50.
            colormap('hot'); colorbar; axis off;
            axis image

            handles.image2(:,:,ii) = Image2(:,:,ii); % Transfer Image2 to the handle.
            guidata(hObject, handles);          
        end
        
        guidata(hObject, handles);          % May not be necessary.  
        
        %%=================== close all shutters =======================
        s = daq.createSession('ni'); %Create a Session;
        addDigitalChannel(s,'Dev2','Port0/Line1:3','OutputOnly'); % 3 for Confocal 640 nm, 2 for Tisa, 1 for 473.
        addDigitalChannel(s,'Dev2','Port0/Line5','OutputOnly'); % 5 is for 561 and 473 nm shutters.
        addDigitalChannel(s,'Dev2','Port0/Line7','OutputOnly'); % Start trigger of bh board
        outputSingleScan(s,[0 0 0 0 0]);
        stop(s);  release(s);
        %%========= end of shutter settings ========

        zout = daq.createSession('ni'); % create a session to apply voltage.
        addAnalogOutputChannel(zout,'Dev2',2,'Voltage'); % The channel that control the z position.
        outputSingleScan(zout,init_z); % Send the measured voltage before scanning starts to the z piezo.
        stop(zout);
        release(zout);
        
        %state declaration
        set(handles.text10,'string','Scanning finished!'); 
        set(handles.text10,'BackgroundColor','y'); 
        disp('finished!');
        
        %==================== Save the recorded images automatically.
        autosave_value = get(handles.AutoSave_1, 'Value');
        if(autosave_value)
            folder = get(handles.Path_1, 'String');
            name1 = 'scan_1_image_1.tif';
            name2 = 'scan_1_image_2.tif';
            full_path1 = strcat(folder, name1);
            full_path2 = strcat(folder, name2);
            options.message = false; % Show the message in the message box, not in the com window.
            saveastiff(uint16(Image1),full_path1,options);
            saveastiff(uint16(Image2),full_path2,options);
        end
        %----============Autosave finishes ---
        
    end

% --- Executes during object creation, after setting all properties.
% Axial scan range
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Axial scan range
function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double

% Measurement time for FCS
function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double

% --- Executes during object creation, after setting all properties.
% Measurement time for FCS
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in scan_mode1.
% Scan mode for FCS. scan_mode1 is one-focus scan (1s/pixel).
function scan_mode1_Callback(hObject, eventdata, handles)
% hObject    handle to scan_mode1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit1, 'string','1'); 
% Hint: get(hObject,'Value') returns toggle state of scan_mode1


% --- Executes during object creation, after setting all properties.
% Axial step for 3D imaging.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Axial step for 3D imaging.
function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double

% --- Executes during object creation, after setting all properties.
% Number of lines to average.
function NumAveLine_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NumAveLine (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Number of lines to average.
function NumAveLine_Callback(hObject, eventdata, handles)
% hObject    handle to NumAveLine (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NumAveLine as text
%        str2double(get(hObject,'String')) returns contents of NumAveLine as a double

% --- Executes on button press in StopBtn.
% --- Stop button for scanning
function StopBtn_Callback(hObject, eventdata, handles)
% hObject    handle to StopBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if(handles.Image_model < 4) % 2D scans.
    stop(handles.s);
    release(handles.s);
else
    set(handles.StopBtn,'userdata',1); % To stop the z scan loop
    stop(handles.s);
    release(handles.s);
end

function XLength_Callback(hObject, eventdata, handles)
% hObject    handle to XLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of XLength as text
%        str2double(get(hObject,'String')) returns contents of XLength as a double
PxSizeX_1_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function XLength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to XLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function YPixNum_Callback(hObject, eventdata, handles)
% hObject    handle to YPixNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of YPixNum as text
%        str2double(get(hObject,'String')) returns contents of YPixNum as a double


% --- Executes during object creation, after setting all properties.
function YPixNum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YPixNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function YLength_Callback(hObject, eventdata, handles)
% hObject    handle to YLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of YLength as text
%        str2double(get(hObject,'String')) returns contents of YLength as a double
PxSizeY_1_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function YLength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Save_Image1.
function Save_Image1_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Image1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fp = 'D:\Weichun\Measurements';
fn = 'NI_image_m1';
img_fn = fullfile(fp,strcat(fn,'.tif'));
[img_fn, img_fp] = uiputfile(img_fn, 'Save image as ...');

if (~isequal(img_fn,0) && ~isequal(img_fp,0))
    set(handles.text10,'string','Saving image ...'); 
    set(handles.text10,'BackgroundColor','w');
    drawnow();
    tic
    options.message = false; % Show the message in the message box, not in the com window.
    saveastiff(uint16(handles.image1),fullfile(img_fp,img_fn),options);
    t = toc;
    msgstr = sprintf('The image was saved successfully. Elapsed time: %.3f s',t); 
    set(handles.text10,'String',msgstr); 
    set(handles.text10,'BackgroundColor','w');
    drawnow();
end

% --- Executes on button press in Save_Data1.
function Save_Data1_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Data1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fp = 'D:\Weichun\Measurements';
fn = 'NI_image_m1';
img_fn = fullfile(fp,strcat(fn,'.mat'));
[img_fn, img_fp] = uiputfile(img_fn, 'Save data as ...');

if (~isequal(img_fn,0) && ~isequal(img_fp,0))
    set(handles.text10,'string','Saving data ...'); 
    set(handles.text10,'BackgroundColor','w');
    drawnow();
    tic
    
    image = handles.image1;
    save(fullfile(img_fp,img_fn), 'image');

    t = toc;
    msgstr = sprintf('The data were saved successfully. Elapsed time: %.3f s',t); 
    set(handles.text10,'String',msgstr); 
    set(handles.text10,'BackgroundColor','w');
    drawnow();
end


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in ScanAxis_1.
function ScanAxis_1_Callback(hObject, eventdata, handles)
% hObject    handle to ScanAxis_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ScanAxis_1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ScanAxis_1


% --- Executes during object creation, after setting all properties.
function ScanAxis_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ScanAxis_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ScanAxis_2.
function ScanAxis_2_Callback(hObject, eventdata, handles)
% hObject    handle to ScanAxis_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ScanAxis_2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ScanAxis_2


% --- Executes during object creation, after setting all properties.
function ScanAxis_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ScanAxis_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ScanAxis_3.
function ScanAxis_3_Callback(hObject, eventdata, handles)
% hObject    handle to ScanAxis_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ScanAxis_3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ScanAxis_3


% --- Executes during object creation, after setting all properties.
function ScanAxis_3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ScanAxis_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ZPixNum_Callback(hObject, eventdata, handles)
% hObject    handle to ZPixNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ZPixNum as text
%        str2double(get(hObject,'String')) returns contents of ZPixNum as a double


% --- Executes during object creation, after setting all properties.
function ZPixNum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ZPixNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ZLength_Callback(hObject, eventdata, handles)
% hObject    handle to ZLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ZLength as text
%        str2double(get(hObject,'String')) returns contents of ZLength as a double


% --- Executes during object creation, after setting all properties.
function ZLength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ZLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Shutter640.
function Shutter640_Callback(hObject, eventdata, handles)
% hObject    handle to Shutter640 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Shutter640


% --- Executes on button press in TiSaShutter.
function TiSaShutter_Callback(hObject, eventdata, handles)
% hObject    handle to TiSaShutter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of TiSaShutter


% --- Executes on button press in Shutter561.
function Shutter561_Callback(hObject, eventdata, handles)
% hObject    handle to Shutter561 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Shutter561


% --- Executes on button press in Save_Image2.
function Save_Image2_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Image2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fp = 'D:\Weichun\Measurements';
fn = 'NI_image_m2';
img_fn = fullfile(fp,strcat(fn,'.tif'));
[img_fn, img_fp] = uiputfile(img_fn, 'Save image as ...');

if (~isequal(img_fn,0) && ~isequal(img_fp,0))
    set(handles.text10,'string','Saving image ...'); 
    set(handles.text10,'BackgroundColor','w');
    drawnow();
    tic
    options.message = false; % Show the message in the message box, not in the com window.
    saveastiff(uint16(handles.image2),fullfile(img_fp,img_fn),options);
    t = toc;
    msgstr = sprintf('The image was saved successfully. Elapsed time: %.3f s',t); 
    set(handles.text10,'String',msgstr); 
    set(handles.text10,'BackgroundColor','w');
    drawnow();
end


% --- Executes on button press in Save_Data2.
function Save_Data2_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Data2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fp = 'D:\Weichun\Measurements';
fn = 'NI_image_m2';
img_fn = fullfile(fp,strcat(fn,'.mat'));
[img_fn, img_fp] = uiputfile(img_fn, 'Save data as ...');

if (~isequal(img_fn,0) && ~isequal(img_fp,0))
    set(handles.text10,'string','Saving data ...'); 
    set(handles.text10,'BackgroundColor','w');
    drawnow();
    tic
    
    image = handles.image2;
    save(fullfile(img_fp,img_fn), 'image');

    t = toc;
    msgstr = sprintf('The data were saved successfully. Elapsed time: %.3f s',t); 
    set(handles.text10,'String',msgstr); 
    set(handles.text10,'BackgroundColor','w');
    drawnow();
end


% --- Executes on button press in Shutter473.
function Shutter473_Callback(hObject, eventdata, handles)
% hObject    handle to Shutter473 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Shutter473


% --- Executes on selection change in ScanAxis_1.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to ScanAxis_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ScanAxis_1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ScanAxis_1


% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ScanAxis_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ScanAxis_2.
function popupmenu5_Callback(hObject, eventdata, handles)
% hObject    handle to ScanAxis_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ScanAxis_2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ScanAxis_2


% --- Executes during object creation, after setting all properties.
function popupmenu5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ScanAxis_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ScanAxis_3.
function popupmenu6_Callback(hObject, eventdata, handles)
% hObject    handle to ScanAxis_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ScanAxis_3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ScanAxis_3


% --- Executes during object creation, after setting all properties.
function popupmenu6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ScanAxis_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in StartBtn.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to StartBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in StopBtn.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to StopBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function PixTime_2_Callback(hObject, eventdata, handles)
% hObject    handle to PixTime_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PixTime_2 as text
%        str2double(get(hObject,'String')) returns contents of PixTime_2 as a double


% --- Executes during object creation, after setting all properties.
function PixTime_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PixTime_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function XPixNum_2_Callback(hObject, eventdata, handles)
% hObject    handle to XPixNum_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of XPixNum_2 as text
%        str2double(get(hObject,'String')) returns contents of XPixNum_2 as a double


% --- Executes during object creation, after setting all properties.
function XPixNum_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to XPixNum_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function XLength_2_Callback(hObject, eventdata, handles)
% hObject    handle to XLength_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of XLength_2 as text
%        str2double(get(hObject,'String')) returns contents of XLength_2 as a double
PxSizeX_2_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function XLength_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to XLength_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function YPixNum_2_Callback(hObject, eventdata, handles)
% hObject    handle to YPixNum_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of YPixNum_2 as text
%        str2double(get(hObject,'String')) returns contents of YPixNum_2 as a double


% --- Executes during object creation, after setting all properties.
function YPixNum_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YPixNum_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function YLength_2_Callback(hObject, eventdata, handles)
% hObject    handle to YLength_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of YLength_2 as text
%        str2double(get(hObject,'String')) returns contents of YLength_2 as a double
PxSizeY_2_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function YLength_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YLength_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ZPixNum_2_Callback(hObject, eventdata, handles)
% hObject    handle to ZPixNum_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ZPixNum_2 as text
%        str2double(get(hObject,'String')) returns contents of ZPixNum_2 as a double


% --- Executes during object creation, after setting all properties.
function ZPixNum_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ZPixNum_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ZLength_2_Callback(hObject, eventdata, handles)
% hObject    handle to ZLength_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ZLength_2 as text
%        str2double(get(hObject,'String')) returns contents of ZLength_2 as a double


% --- Executes during object creation, after setting all properties.
function ZLength_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ZLength_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in AveLine_2.
function AveLine_2_Callback(hObject, eventdata, handles)
% hObject    handle to AveLine_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AveLine_2



function NumAveLine_2_Callback(hObject, eventdata, handles)
% hObject    handle to NumAveLine_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NumAveLine_2 as text
%        str2double(get(hObject,'String')) returns contents of NumAveLine_2 as a double


% --- Executes during object creation, after setting all properties.
function NumAveLine_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NumAveLine_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Shutter640_2.
function Shutter640_2_Callback(hObject, eventdata, handles)
% hObject    handle to Shutter640_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Shutter640_2


% --- Executes on button press in TiSaShutter_2.
function TiSaShutter_2_Callback(hObject, eventdata, handles)
% hObject    handle to TiSaShutter_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of TiSaShutter_2


% --- Executes on button press in Shutter561_2.
function Shutter561_2_Callback(hObject, eventdata, handles)
% hObject    handle to Shutter561_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Shutter561_2


% --- Executes on button press in Shutter473_2.
function Shutter473_2_Callback(hObject, eventdata, handles)
% hObject    handle to Shutter473_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Shutter473_2


% --- Executes on selection change in ScanAxis2_1.
function ScanAxis2_1_Callback(hObject, eventdata, handles)
% hObject    handle to ScanAxis2_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ScanAxis2_1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ScanAxis2_1


% --- Executes during object creation, after setting all properties.
function ScanAxis2_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ScanAxis2_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ScanAxis2_2.
function ScanAxis2_2_Callback(hObject, eventdata, handles)
% hObject    handle to ScanAxis2_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ScanAxis2_2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ScanAxis2_2


% --- Executes during object creation, after setting all properties.
function ScanAxis2_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ScanAxis2_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ScanAxis2_3.
function ScanAxis2_3_Callback(hObject, eventdata, handles)
% hObject    handle to ScanAxis2_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ScanAxis2_3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ScanAxis2_3


% --- Executes during object creation, after setting all properties.
function ScanAxis2_3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ScanAxis2_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in StartBtn2.
function StartBtn2_Callback(hObject, eventdata, handles)
% hObject    handle to StartBtn2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    set(handles.StopBtn, 'userdata', 0);
    
    %%===================shutters and trigger setting=======================
    s = daq.createSession('ni'); %Create a Session;
    addDigitalChannel(s,'Dev2','Port0/Line1:3','OutputOnly'); % 3 for Confocal 640 nm, 2 for Tisa, 1 for 473 nm.
    addDigitalChannel(s,'Dev2','Port0/Line5','OutputOnly'); % 5 is for 561 and 473 nm shutters.
    addDigitalChannel(s,'Dev2','Port0/Line7','OutputOnly'); % Start trigger of bh board
    shutter473_value = get(handles.Shutter473_2, 'Value');
    shutter561_value = get(handles.Shutter561_2, 'Value');
    shutter640_value = get(handles.Shutter640_2, 'Value');
    shuttertisa_value = get(handles.TiSaShutter_2, 'Value');
    StartTrigger = get(handles.StartBH_2, 'Value'); % Start trigger for bh card.
    outputSingleScan(s,[shutter473_value, shuttertisa_value, shutter640_value, shutter561_value, StartTrigger]);
    stop(s);  release(s);
    %%========= end of shutter settings ========
    
    Axis_1_list = get(handles.ScanAxis2_1,'String'); % scanning axes.
    Axis_1_value = get(handles.ScanAxis2_1,'Value'); % scanning axes.
    Axis_1 = char(Axis_1_list(Axis_1_value));
    
    Axis_2_list = get(handles.ScanAxis2_2,'String'); % scanning axes.
    Axis_2_value = get(handles.ScanAxis2_2,'Value'); % scanning axes.
    Axis_2 = char(Axis_2_list(Axis_2_value));
    
    Axis_3_list = get(handles.ScanAxis2_3,'String'); % scanning axes.
    Axis_3_value = get(handles.ScanAxis2_3,'Value'); % scanning axes.
    Axis_3 = char(Axis_3_list(Axis_3_value));
    
    Pixel_dwelltime = get(handles.PixTime_2, 'string');
    Pixel_dwelltime = str2double(Pixel_dwelltime);
    Pixel_dwelltime = Pixel_dwelltime*1e-6; %Scanning period; Unit: microsecond; convert to second.
    
    Num_Pixel_X = str2double(get(handles.XPixNum_2, 'string'));
    Num_Pixel_Y = str2double(get(handles.YPixNum_2, 'string'));
    Num_Pixel_Z = str2double(get(handles.ZPixNum_2, 'string'));

    XLength = str2double(get(handles.XLength_2, 'string')); % scan area.
    YLength = str2double(get(handles.YLength_2, 'string')); % scan area.
    ZLength = str2double(get(handles.ZLength_2, 'string')); % scan area.

    % Scan_diameter_axial = str2double(get(handles.edit7, 'string')); % axial scan range (um)
    
    % Num_Pixel_axial = str2double(get(handles.edit9,'string'));
%     handles.Num_Pixel_axial = Num_Pixel_axial;
%     guidata(hObject, handles);
    
    Num_line_aver = str2double(get(handles.NumAveLine_2,'string')); % Number of lines to average
    Line_aver = get(handles.AveLine_2, 'value');  %0 for averaging, and 1 for not doing averaging.
    
    if Line_aver == 0
        Num_line_aver = 1;
    end 
    
    switch Axis_1
        case 'X Axis'
            Model_1 = 1;
        case 'Y Axis'
            Model_1 = 2;
        case 'Z Axis'
            Model_1 = 3;
        case 'None'
            Model_1 = 4;
    end
    
    switch Axis_2
        case 'X Axis'
            Model_2 = 1;
        case 'Y Axis'
            Model_2 = 2;
        case 'Z Axis'
            Model_2 = 3;
        case 'None'
            Model_2 = 4;
    end
    
    switch Axis_3
        case 'X Axis'
            Model_3 = 1;
        case 'Y Axis'
            Model_3 = 2;
        case 'Z Axis'
            Model_3 = 3;
        case 'None'
            Model_3 = 4;
    end
    
    if(Model_1 == 1 && Model_2 == 2 && Model_3 == 4)
        Image_model = 1; % XY
    elseif(Model_1 == 1 && Model_2 == 3 && Model_3 == 4)
        Image_model = 2; % XZ
    elseif(Model_1 == 2 && Model_2 == 3 && Model_3 == 4)
        Image_model = 3; % YZ
    elseif(Model_1 == 1 && Model_2 == 2 && Model_3 == 3)
        Image_model = 4; % XYZ
    elseif(Model_1 == 1 && Model_2 == 3 && Model_3 == 2)
        Image_model = 5; % XZY
    elseif(Model_1 == 2 && Model_2 == 3 && Model_3 == 1)
        Image_model = 6; % YZX
    end
    
    handles.Image_model = Image_model;
    guidata(hObject, handles);
   
    if(Image_model == 1) % XY 2D scan
          
        % devices = daq.getDevices; %discover a device
        % daq.getDevices; %discover a device; Not really needed.
        % devices(1), 
        % Create a Session: 
        s = daq.createSession('ni'); %Create a Session;
        handles.s = s; % Transfer s to the handle.
        guidata(hObject, handles);
        addAnalogOutputChannel(s,'Dev2',0,'Voltage'); 
        addAnalogOutputChannel(s,'Dev2',1,'Voltage'); 
        % addAnalogOutputChannel(startbtn2,'Dev2',2,'Voltage');
        % Only 2D scanning, so this line is not necessary.
        % addAnalogOutputChannel(startbtn2,'Dev2',3,'Voltage'); 
        % What is this line doing?
        % This is frame trigger, which is not needed for 2D imaging.
        % The NI board PCI6259 supports in total 4 AO ports.
        
        % ch = addCounterOutputChannel(startbtn2,'Dev2',0, 'PulseGeneration'); % Trigger output; line trigger.
        % Physically it is on the BNC2110 board.
        % Counter1 = addCounterInputChannel(startbtn2,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        % The variable "Counter1" is not used. Change it to the following
        % code.
        addDigitalChannel(s,'Dev2','Port0/Line8','OutputOnly'); 
        % Line trigger, Port0/Line8 is on the NI extension board.
        
        addCounterInputChannel(s,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        addCounterInputChannel(s,'Dev2','ctr0','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
%         addDigitalChannel(startbtn2,'Dev2','Port0/Line8','OutputOnly'); 
%         % Line trigger, Port0/Line8 is on the NI extension board.
        
        
        %Set the Session Rate; 
        Num_Line = Num_Pixel_Y;  % Line number; 
        
        Line_dwlltime = Pixel_dwelltime * Num_Pixel_X; %Scanning period; Unit: s; 
        Frame_dwelltime = Pixel_dwelltime*(Num_Pixel_X*Num_Line)*Num_line_aver; %Unit: s; 
        Num_Total_pixel = Num_Pixel_X*Num_Line*Num_line_aver; %Pixel number; 
        
        s.Rate = 1/Pixel_dwelltime; %Scanning rate; 
        t = linspace(0,Frame_dwelltime,Num_Total_pixel); %Time base; 
        t = t'; 
        
        %Rectangular scan: 
        %x scan % scan voltage to output.
        angle0 = 2*pi*t/Line_dwlltime;
        angle0 = angle(exp(1i*angle0+1i*pi))/pi; %Range: [-1,1]; 
        outputSignal1 = XLength * 0.01035 * angle0 - 0.275; %-0.2750; %x scan; 
        % The numbers were adjusted such that the imaged area is the same
        % as shown in Imspector.
        %y scan        
        angle0 = (floor(t/(Line_dwlltime * Num_line_aver)) - Num_Line/2)/(Num_Line/2); %Range: [-1,1]; 
        outputSignal2 = YLength*0.01035*angle0-0.343; %-0.3430; %y scan; D;     
        % the size of outputSignal1 and outputSignal2 is Num_Total_pixelx1.
        
        %z scan: z scan is not done.
        % z_scan = linspace(-Scan_diameter_axial/2,Scan_diameter_axial/2,Num_Pixel_axial); %Axial scanning range;
        % z1 = z_scan(1);
        % outputSignal3=z1*0.0975*ones(size(outputSignal1))+5.484; %Control aixal position;4.8(in the beginning)  4.8975
        % There is no outputSignal3.
        
        % Frame-trigger: 
        % outputSignal4 = ones(size(outputSignal1)); outputSignal4 = -1*outputSignal4; outputSignal4(1) = 4; %Frame trigger;
        % It does not look doing any serious thing.
        
        %Queue the data:         
%         queueOutputData(startbtn2,[outputSignal1(1:1e4) outputSignal2(1:1e4) outputSignal4(1:1e4)]); %Analogy output; 
        % Device.IsContinuous = true;
        % startbtn2.IsContinuous = true; % It should be like this.
        % The startForeground method cannot be called with IsContinuous set to true.  Use startBackground.
        
        outputSignal3 = zeros(Num_Pixel_Y, Num_Pixel_X * Num_line_aver);
        outputSignal3(:,1) = ones(Num_Pixel_Y, 1); % Line trigger. High level on the first pixel.
        outputSignal3 = reshape(outputSignal3', [Num_Total_pixel, 1]);
        
        % Status message
        set(handles.text10,'string','Scanning 2D XY image...'); 
        set(handles.text10,'BackgroundColor','r'); 
        
        %[captured_data,time] = startbtn2.startForeground(); 
        disp('start at this moment'); 
%         Data =startForeground(startbtn2); %Start the Session in Foreground;         
%         Data=diff(Data,1,1); Data=[Data;0]; %Data;         
%         Data=reshape(Data,[sqrt(length(Data)),sqrt(length(Data))])'; 
%         %Data=reshape(Data,[Num_Pixel*Num_line_aver,Num_Line])'; 
%         gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Data); colormap('hot'); colorbar; axis off;
%         
%         %Time series of the interferograms:      
%         %z_scan=linspace(-Scan_diameter_axial/2,Scan_diameter_axial/2,Num_Pixel_axial); %Axial scanning range; 
%         
%         %for ii=1:Num_Pixel_axial,           
%         %stop(startbtn2); release(startbtn2);              
%         %z1=z_scan(ii);
%         %outputSignal3=z1*0.0975*ones(size(outputSignal1))+5.3984; %Control aixal position;4.8(in the beginning) 0,384  
%         stop(startbtn2);  release(startbtn2);
          % queueOutputData(startbtn2,[outputSignal1 outputSignal2 outputSignal4]); %Analogy output;  
        queueOutputData(s,[outputSignal1 outputSignal2 outputSignal3]); % Signal output;
        % When generating output signals, you must queue data before you call startForeground or startBackground.

        % Clocked Counter Output(to synchronize line trigger): 
        % It generates a pulse train with a defined frequency during the
        % scanning. So it ends up with more line triggers than expected
        % because of the extra time during scanner flying back! (not sure
        % of this)
        % ch = startbtn2.Channels(4); % addCounterOutputChannel, channel for line
        % trigger output. ch is defined before.
        
        % ch.Frequency = 1/(Line_dwlltime*Num_line_aver); % Trigger output frequency;
        
        % Here I should add the scaner fly back time. How much is it?
        % A line trigger is generated after the lines to average are
        % finished.
        % ch.InitialDelay = Line_dwlltime/0.95; % Innitial delay; % What is this doing? Why 1.98?
        % The delay factor can be adjusted according to the pixel number
        % and px dwell time.
        % It is better not to use any delay. 
        
        % ch.DutyCycle = 1e-4;  %Duty cycle; 10ns Pulse width;
        
        % Line trigger pulse width = Line_dwlltime*Num_line_aver *
        % ch.DutyCycle
        
        Data = startForeground(s); %Start the Session in Foreground;
        % startBackground(startbtn2) does not have output arguments.
        
        stop(s);  release(s);
                %%=================== close all shutters =======================
        s = daq.createSession('ni'); %Create a Session;
        addDigitalChannel(s,'Dev2','Port0/Line1:3','OutputOnly'); % 3 for Confocal 640 nm, 2 for Tisa, 1 for 473.
        addDigitalChannel(s,'Dev2','Port0/Line5','OutputOnly'); % 5 is for 561 and 473 nm shutters.
        addDigitalChannel(s,'Dev2','Port0/Line7','OutputOnly'); % make the bh trigger signal low
        outputSingleScan(s,[0 0 0 0 0]);
        stop(s);  release(s);
        %%========= end of shutter settings ========
        %end
        
        % Show the aquired image -- channel red
        Data1 = diff(Data(:,1),1,1);
        Data1 = [Data1; 0]; %Data; 
        Image1 = reshape(Data1,[Num_Pixel_X*Num_line_aver, Num_Line])'; %Interferogram 1;        
        gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Image1); 
        % caxis([0,50]); % Force the maximum intensity to be 50.
        colormap('hot'); colorbar; axis off;
        axis image
        
        handles.image1 = Image1; % Transfer Image1 to the handle.
        guidata(hObject, handles); % This is for saving the image.
        
        % Show the aquired image -- channel blue
        Data2 = diff(Data(:,2),1,1); Data2 = [Data2; 0]; %Data; 
        Image2 = reshape(Data2,[Num_Pixel_X*Num_line_aver, Num_Line])'; %Interferogram 1;        
        gcf; set(gcf,'currentaxes',handles.axes2); imagesc(Image2); 
        % caxis([0,50]); % Force the maximum intensity to be 50.
        colormap('hot'); colorbar; axis off;
        axis image
        
        handles.image2 = Image2; % Transfer Image2 to the handle.
        guidata(hObject, handles);

        %state declaration
        set(handles.text10,'string','Scanning finished!'); 
        set(handles.text10,'BackgroundColor','y'); 
        disp('Finished!');
        
        %==================== Save the recorded images automatically.
        autosave_value = get(handles.AutoSave_2, 'Value');
        if(autosave_value)
            folder = get(handles.Path_2, 'String');
            name1 = 'scan_2_image_1.tif';
            name2 = 'scan_2_image_2.tif';
            full_path1 = strcat(folder, name1);
            full_path2 = strcat(folder, name2);
            options.message = false; % Show the message in the message box, not in the com window.
            saveastiff(uint16(Image1),full_path1,options);
            saveastiff(uint16(Image2),full_path2,options);
        end
        %----============Autosave finishes ---
    
    elseif(Image_model == 2) % XZ 2D scan
          
        % devices = daq.getDevices; %discover a device
        % daq.getDevices; %discover a device; Not really needed.
        % devices(1), 
        % Create a Session: 
        s = daq.createSession('ni'); %Create a Session;
        handles.s = s; % Transfer s to the handle.
        guidata(hObject, handles);
        addAnalogOutputChannel(s,'Dev2',0,'Voltage'); % X axis
        addAnalogOutputChannel(s,'Dev2',2,'Voltage'); % Z axis
        % XZ scan
        addDigitalChannel(s,'Dev2','Port0/Line8','OutputOnly'); 
        % Line trigger, Port0/Line8 is on the NI extension board.
        
        % ch = addCounterOutputChannel(startbtn2,'Dev2',0, 'PulseGeneration'); % Trigger output; line trigger.
        % Physically it is on the BNC2110 board.
        % Counter1 = addCounterInputChannel(startbtn2,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        % The variable "Counter1" is not used. Change it to the following code.
        
        addCounterInputChannel(s,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        addCounterInputChannel(s,'Dev2','ctr0','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
%         addDigitalChannel(startbtn2,'Dev2','Port0/Line8','OutputOnly'); 
%         % Line trigger, Port0/Line8 is on the NI extension board.
        

        %Set the Session Rate; 
        Num_Line = Num_Pixel_Z;  % Line number; 
        
        Line_dwlltime = Pixel_dwelltime * Num_Pixel_X; % Scanning period; Unit: s; 
        Frame_dwelltime = Pixel_dwelltime*(Num_Pixel_X*Num_Line)*Num_line_aver; %Unit: s; 
        Num_Total_pixel = Num_Pixel_X*Num_Line*Num_line_aver; %Pixel number; 
        
        s.Rate = 1/Pixel_dwelltime; %Scanning rate; 
        t = linspace(0,Frame_dwelltime,Num_Total_pixel); %Time base; 
        t = t'; 
        
        %Rectangular scan: 
        %x scan % scan voltage to output.
        angle0 = 2*pi*t/Line_dwlltime;
        angle0 = angle(exp(1i*angle0+1i*pi))/pi; %Range: [-1,1]; 
        outputSignal1 = XLength * 0.01035 * angle0 - 0.275; %-0.2750; %x scan; 
        % The numbers were adjusted such that the imaged area is the same
        % as shown in Imspector.
        %y scan        
        angle0 = (floor(t/(Line_dwlltime * Num_line_aver)) - Num_Line/2)/(Num_Line/2); %Range: [-1,1]; 
        outputSignal2 = ZLength*0.0480*angle0 + 4.6159; % z scan
        % the size of outputSignal1 and outputSignal2 is Num_Total_pixelx1.
        
        outputSignal3 = zeros(Num_Pixel_Z, Num_Pixel_X*Num_line_aver);
        outputSignal3(:,1) = ones(Num_Pixel_Z,1); % Line trigger. High level on the first pixel.
        outputSignal3 = reshape(outputSignal3',[Num_Total_pixel, 1]);
        
        % Status message
        set(handles.text10,'string','Scanning 2D XZ image...'); 
        set(handles.text10,'BackgroundColor','r'); 
        
        %[captured_data,time] = startbtn2.startForeground(); 
        disp('start at this moment'); 
%         Data =startForeground(startbtn2); %Start the Session in Foreground;         
%         Data=diff(Data,1,1); Data=[Data;0]; %Data;         
%         Data=reshape(Data,[sqrt(length(Data)),sqrt(length(Data))])'; 
%         %Data=reshape(Data,[Num_Pixel*Num_line_aver,Num_Line])'; 
%         gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Data); colormap('hot'); colorbar; axis off;
%         
%         %Time series of the interferograms:      
%         %z_scan=linspace(-Scan_diameter_axial/2,Scan_diameter_axial/2,Num_Pixel_axial); %Axial scanning range; 
%         
%         %for ii=1:Num_Pixel_axial,           
%         %stop(startbtn2); release(startbtn2);              
%         %z1=z_scan(ii);
%         %outputSignal3=z1*0.0975*ones(size(outputSignal1))+5.3984; %Control aixal position;4.8(in the beginning) 0,384  
%         stop(startbtn2);  release(startbtn2);
          % queueOutputData(startbtn2,[outputSignal1 outputSignal2 outputSignal4]); %Analogy output;  
        queueOutputData(s,[outputSignal1 outputSignal2 outputSignal3]); % Signal output;

        % Clocked Counter Output(to synchronize line trigger): 
        % It generates a pulse train with a defined frequency during the
        % scanning. So it ends up with more line triggers than expected
        % because of the extra time during scanner flying back!
        % ch = startbtn2.Channels(4); % addCounterOutputChannel, channel for line
        % trigger output. ch is defined before.
        
        % ch.Frequency = 1/(Line_dwlltime*Num_line_aver); % Trigger output frequency;
        
        % Here I should add the scaner fly back time. How much is it?
        % A line trigger is generated after the lines to average are
        % finished.
        % ch.InitialDelay = Line_dwlltime/0.95; % Innitial delay; % What is this doing? Why 1.98?
        % The delay factor can be adjusted according to the pixel number
        % and px dwell time.
        % It is better not to use any delay. 
        
        % ch.DutyCycle = 1e-4;  %Duty cycle; 10ns Pulse width;
        
        % Line trigger pulse width = Line_dwlltime*Num_line_aver *
        % ch.DutyCycle
        
        Data = startForeground(s); %Start the Session in Foreground;
        
        stop(s);  release(s);
        
                %%=================== close all shutters =======================
        s = daq.createSession('ni'); %Create a Session;
        addDigitalChannel(s,'Dev2','Port0/Line1:3','OutputOnly'); % 3 for Confocal 640 nm, 2 for Tisa, 1 for 473
        addDigitalChannel(s,'Dev2','Port0/Line5','OutputOnly'); % 5 is for 561 and 473 nm shutters.
        addDigitalChannel(s,'Dev2','Port0/Line7','OutputOnly'); % Start trigger of bh board
        outputSingleScan(s,[0 0 0 0 0]);
        stop(s);  release(s);
        %%========= end of shutter settings ========
        %end
        
        
        % Show the aquired image -- channel red
        Data1 = diff(Data(:,1),1,1);
        Data1 = [Data1; 0]; %Data; 
        Image1 = reshape(Data1,[Num_Pixel_X*Num_line_aver, Num_Line])'; %Interferogram 1;        
        gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Image1); 
        % caxis([0,50]); % Force the maximum intensity to be 50.
        colormap('hot'); colorbar; axis off;
        axis image
        
        handles.image1 = Image1; % Transfer Image1 to the handle.
        guidata(hObject, handles); % This is for saving the image.
        
        % Show the aquired image -- channel blue
        Data2 = diff(Data(:,2),1,1); Data2 = [Data2; 0]; %Data; 
        Image2 = reshape(Data2,[Num_Pixel_X*Num_line_aver, Num_Line])'; %Interferogram 1;        
        gcf; set(gcf,'currentaxes',handles.axes2); imagesc(Image2); 
        % caxis([0,50]); % Force the maximum intensity to be 50.
        colormap('hot'); colorbar; axis off;
        axis image
        
        handles.image2 = Image2; % Transfer Image2 to the handle to save the image manually later.
        guidata(hObject, handles);


        %state declaration
        set(handles.text10,'string','Scanning finished!'); 
        set(handles.text10,'BackgroundColor','y'); 
        disp('Finished!');
        
        %==================== Save the recorded images automatically.
        autosave_value = get(handles.AutoSave_2, 'Value');
        if(autosave_value)
            folder = get(handles.Path_2, 'String');
            name1 = 'scan_2_image_1.tif';
            name2 = 'scan_2_image_2.tif';
            full_path1 = strcat(folder, name1);
            full_path2 = strcat(folder, name2);
            options.message = false; % Show the message in the message box, not in the com window.
            saveastiff(uint16(Image1),full_path1,options);
            saveastiff(uint16(Image2),full_path2,options);
        end
        %----============Autosave finishes ---
        
    elseif(Image_model == 3) % YZ 2D scan
          
        % devices = daq.getDevices; %discover a device
        % daq.getDevices; %discover a device; Not really needed.
        % devices(1), 
        % Create a Session: 
        s = daq.createSession('ni'); %Create a Session;
        handles.s = s; % Transfer s to the handle.
        guidata(hObject, handles);
        addAnalogOutputChannel(s,'Dev2',1,'Voltage'); 
        addAnalogOutputChannel(s,'Dev2',2,'Voltage'); 
        % YZ scan
        addDigitalChannel(s,'Dev2','Port0/Line8','OutputOnly'); 
%       % Line trigger
        
        % ch = addCounterOutputChannel(startbtn2,'Dev2',0, 'PulseGeneration'); % Trigger output; line trigger.
        % Physically it is on the BNC2110 board.
        % Counter1 = addCounterInputChannel(startbtn2,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        % The variable "Counter1" is not used. Change it to the following code.
        
        addCounterInputChannel(s,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        addCounterInputChannel(s,'Dev2','ctr0','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
%         addDigitalChannel(startbtn2,'Dev2','Port0/Line8','OutputOnly'); 
%         % Line trigger

        %Set the Session Rate; 
        Num_Line = Num_Pixel_Z;  % Line number; 
        
        Line_dwlltime = Pixel_dwelltime * Num_Pixel_Y; % Scanning period; Unit: s; 
        Frame_dwelltime = Pixel_dwelltime*(Num_Pixel_Y*Num_Line)*Num_line_aver; %Unit: s; 
        Num_Total_pixel = Num_Pixel_Y*Num_Line*Num_line_aver; %Pixel number; 
        
        s.Rate = 1/Pixel_dwelltime; %Scanning rate; 
        t = linspace(0,Frame_dwelltime,Num_Total_pixel); %Time base; 
        t = t'; 
        
        %Rectangular scan: 
        %x scan % scan voltage to output.
        angle0 = 2*pi*t/Line_dwlltime;
        angle0 = angle(exp(1i*angle0+1i*pi))/pi; %Range: [-1,1]; 
        outputSignal1 = YLength * 0.01035 * angle0 -0.343; 
        % The numbers were adjusted such that the imaged area is the same
        % as shown in Imspector.
        %y scan        
        angle0 = (floor(t/(Line_dwlltime * Num_line_aver)) - Num_Line/2)/(Num_Line/2); %Range: [-1,1]; 
        outputSignal2 = ZLength*0.0480*angle0 + 4.6159; % z scan
        % the size of outputSignal1 and outputSignal2 is Num_Total_pixelx1.
        
        outputSignal3 = zeros(Num_Pixel_Z, Num_Pixel_Y*Num_line_aver);
        outputSignal3(:,1) = ones(Num_Pixel_Z,1); % Line trigger. High level on the first pixel.
        outputSignal3 = reshape(outputSignal3',[Num_Total_pixel, 1]);
        
        % Status message
        set(handles.text10,'string','Scanning 2D YZ image...'); 
        set(handles.text10,'BackgroundColor','r'); 
        
        %[captured_data,time] = startbtn2.startForeground(); 
        disp('start at this moment'); 
%         Data =startForeground(startbtn2); %Start the Session in Foreground;         
%         Data=diff(Data,1,1); Data=[Data;0]; %Data;         
%         Data=reshape(Data,[sqrt(length(Data)),sqrt(length(Data))])'; 
%         %Data=reshape(Data,[Num_Pixel*Num_line_aver,Num_Line])'; 
%         gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Data); colormap('hot'); colorbar; axis off;
%         
%         %Time series of the interferograms:      
%         %z_scan=linspace(-Scan_diameter_axial/2,Scan_diameter_axial/2,Num_Pixel_axial); %Axial scanning range; 
%         
%         %for ii=1:Num_Pixel_axial,           
%         %stop(startbtn2); release(startbtn2);              
%         %z1=z_scan(ii);
%         %outputSignal3=z1*0.0975*ones(size(outputSignal1))+5.3984; %Control aixal position;4.8(in the beginning) 0,384  
%         stop(startbtn2);  release(startbtn2);
          % queueOutputData(startbtn2,[outputSignal1 outputSignal2 outputSignal4]); %Analogy output;  
        queueOutputData(s,[outputSignal1 outputSignal2 outputSignal3]); % Signal output;

        % Clocked Counter Output(to synchronize line trigger): 
        % It generates a pulse train with a defined frequency during the
        % scanning. So it ends up with more line triggers than expected
        % because of the extra time during scanner flying back!
        % ch = startbtn2.Channels(4); % addCounterOutputChannel, channel for line
        % trigger output. ch is defined before.
        
        % ch.Frequency = 1/(Line_dwlltime*Num_line_aver); % Trigger output frequency;
        
        % Here I should add the scaner fly back time. How much is it?
        % A line trigger is generated after the lines to average are
        % finished.
        % ch.InitialDelay = Line_dwlltime/0.95; % Innitial delay; % What is this doing? Why 1.98?
        % The delay factor can be adjusted according to the pixel number
        % and px dwell time.
        % It is better not to use any delay. 
        
        % ch.DutyCycle = 1e-4;  %Duty cycle; 10ns Pulse width;
        
        % Line trigger pulse width = Line_dwlltime*Num_line_aver *
        % ch.DutyCycle
        
        Data = startForeground(s); %Start the Session in Foreground;
        
        stop(s);  release(s);
        
                %%=================== close all shutters =======================
        s = daq.createSession('ni'); %Create a Session;
        addDigitalChannel(s,'Dev2','Port0/Line1:3','OutputOnly'); % 3 for Confocal 640 nm, 2 for Tisa, 1 for 473.
        addDigitalChannel(s,'Dev2','Port0/Line5','OutputOnly'); % 5 is for 561 and 473 nm shutters.
        addDigitalChannel(s,'Dev2','Port0/Line7','OutputOnly'); % Start trigger of bh board
        outputSingleScan(s,[0 0 0 0 0]);
        stop(s);  release(s);
        %%========= end of shutter settings ========
        
        % Show the aquired image -- channel red
        Data1 = diff(Data(:,1),1,1);
        Data1 = [Data1; 0]; %Data; 
        Image1 = reshape(Data1,[Num_Pixel_Y*Num_line_aver, Num_Line])'; %Interferogram 1;        
        gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Image1); 
        % caxis([0,50]); % Force the maximum intensity to be 50.
        colormap('hot'); colorbar; axis off;
        axis image
        
        handles.image1 = Image1; % Transfer Image1 to the handle.
        guidata(hObject, handles); % This is for saving the image.
        
        % Show the aquired image -- channel blue
        Data2 = diff(Data(:,2),1,1); Data2 = [Data2; 0]; %Data; 
        Image2 = reshape(Data2,[Num_Pixel_Y*Num_line_aver, Num_Line])'; %Interferogram 1;        
        gcf; set(gcf,'currentaxes',handles.axes2); imagesc(Image2); 
        % caxis([0,50]); % Force the maximum intensity to be 50.
        colormap('hot'); colorbar; axis off;
        axis image
        
        handles.image2 = Image2; % Transfer Image2 to the handle.
        guidata(hObject, handles);

        %state declaration
        set(handles.text10,'string','Scanning finished!'); 
        set(handles.text10,'BackgroundColor','y'); 
        disp('Finished!');
        
        %==================== Save the recorded images automatically.
        autosave_value = get(handles.AutoSave_2, 'Value');
        if(autosave_value)
            folder = get(handles.Path_2, 'String');
            name1 = 'scan_2_image_1.tif';
            name2 = 'scan_2_image_2.tif';
            full_path1 = strcat(folder, name1);
            full_path2 = strcat(folder, name2);
            options.message = false; % Show the message in the message box, not in the com window.
            saveastiff(uint16(Image1),full_path1,options);
            saveastiff(uint16(Image2),full_path2,options);
        end
        %----============Autosave finishes ---
        
        
    elseif(Image_model == 4) % XYZ 3D imaging
        
        zin = daq.createSession('ni'); % create a session to measure voltage.
        addAnalogInputChannel(zin, 'Dev2', 'ai0', 'Voltage'); % measure the voltage at the z piezo before the scanning starts
        init_z = inputSingleScan(zin); % measures the voltage.
        stop(zin);
        release(zin); % Stop and release the session.
        %For test:         
        % Num_Pixel=256;Pixel_dwelltime=30e-6;Num_line_aver=10;Scan_diameter=20;
        
        % Path_name='D:\Experiment\2017_09_29_tcspc\Phase_imaging\Cell4_130_130um\'; Do not know what it does.  
        % daq.getDevices; %discover a device; not really needed.
        % devices = daq.getDevices; %discover a device
        % devices(1), 
        %Create a Session: 
        s = daq.createSession('ni'); %Create a Session; 
        handles.s = s; % Transfer s to the handle.
        guidata(hObject, handles);      
 
        addAnalogOutputChannel(s,'Dev2',0,'Voltage'); 
        addAnalogOutputChannel(s,'Dev2',1,'Voltage'); 
        addAnalogOutputChannel(s,'Dev2',2,'Voltage'); % The channel that control the z position.
        % z axis
%         addAnalogOutputChannel(startbtn2,'Dev2',3,'Voltage');
        % For the frame triggers
        
        
%         ch = addCounterOutputChannel(startbtn2,'Dev2',0, 'PulseGeneration'); % Line trigger output;
        addDigitalChannel(s, 'Dev2', 'Port0/Line8', 'OutputOnly'); 
        % Line trigger, Port0/Line8 is on the NI extension board.
        % addDigitalChannel(startbtn2, 'Dev2', 'Port0/Line9', 'OutputOnly');
        % Frame trigger, Port0/Line9 is on the NI extension board.
        
        % Counter1 = addCounterInputChannel(startbtn2,'Dev2','ctr1','EdgeCount');
        % Orignal code
%         addCounterInputChannel(startbtn2,'Dev2','ctr1','EdgeCount');
        % Detector signal input.
        addCounterInputChannel(s,'Dev2','ctr1','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        addCounterInputChannel(s,'Dev2','ctr0','EdgeCount'); % The detector that get signal. ctr0 is the blue channel and ctr1 is the red channel.
        
        %Set the Session Rate; 
        % Num_Pixel=Num_Pixel; %Pixel number; Unnecessary
        Num_Line = Num_Pixel_Y;  %Line number; 
        % Pixel_dwelltime=Pixel_dwelltime; %pixel dwell time; Unit: startbtn2;  Unnecessary
        
        Line_dwlltime = Pixel_dwelltime * Num_Pixel_X; %Scanning period; Unit: s; 
        Frame_dwelltime = Pixel_dwelltime*(Num_Pixel_X*Num_Line)*Num_line_aver; %Unit: s; 
        Num_Total_pixel = Num_Pixel_X*Num_Line*Num_line_aver; %Pixel number; 
        
        s.Rate = 1/Pixel_dwelltime; %Scanning rate; 
        t=linspace(0,Frame_dwelltime,Num_Total_pixel); %Time base; 
        t=t'; 
        
        %Rectangular scan: 
        %x scan
        % Scan_diameter = Scan_diameter; %Unit: um; Unnecessary
        angle0 = 2*pi*t/Line_dwlltime; angle0=angle(exp(1i*angle0+1i*pi))/pi; %Range: [-1,1]; 
        outputSignal1 = XLength*0.01035*angle0 - 0.275; %-0.2750; %x scan; 
        % outputSignal1 is a column vector. The size is the total number of
        % pixels in a 2D scan.
        %y scan        
        angle0 = (floor(t/(Line_dwlltime*Num_line_aver))-Num_Line/2)/(Num_Line/2); %Range: [-1,1]; 
        outputSignal2 = YLength*0.01035*angle0 - 0.343; %-0.3430; %y scan; D;         
        %z scan: 
        % z_scan = linspace(-Scan_diameter_axial/2,Scan_diameter_axial/2,Num_Pixel_axial); %Axial scanning range;
        % z1=z_scan(1);
        % outputSignal3 = z1*0.0975*ones(size(outputSignal1)) + 5.484; %Control aixal position;4.8(in the beginning)  4.8975   
        % This line is not defining the z scan range.
        
        outputSignal4 = zeros(Num_Pixel_Y, Num_Pixel_X * Num_line_aver);
        outputSignal4(:,1) = ones(Num_Pixel_Y, 1); % Line trigger. High level on the first pixel.
        outputSignal4 = reshape(outputSignal4', [Num_Total_pixel, 1]);
%         outputSignal4 = repmat(outputSignal4, Num_Pixel_Z, 1);
%         outputSignal4 = reshape(outputSignal4', [Num_Total_pixel*Num_Pixel_Z, 1]);
        % Line triggers on P0.8.
        
%         %Frame-trigger: (generated on AO3 port).
%         % outputSignal4 = ones(size(outputSignal3));
%         outputSignal5 = ones(size(outputSignal1));
%         outputSignal5 = -1*outputSignal5;
%         outputSignal5(1) = 4; %Frame trigger;

        % Frame trigger, generated on P0.9 (line0/port9, on the extension
        % board)
        % Frame triggers can be unavailable if the number of lines in each
        % frame is exactly the same.
%         outputSignal5 = zeros(Num_Total_pixel, 1);
%         outputSignal5(1) = 1; % High level on the first pixel.
        
        %Queue the data: 
        % queueOutputData(startbtn2,[outputSignal1(1:1e4) outputSignal2(1:1e4) outputSignal3(1:1e4) outputSignal4(1:1e4)]); %Analogy output; 
        % Device.IsContinuous = true;
        % startbtn2.IsContinuous = true; % It should be like this.
        % The startForeground method cannot be called with IsContinuous set to true.  Use startBackground.
        
        % Status message
        set(handles.text10,'string','Running'); 
        set(handles.text10,'BackgroundColor','r'); 
        
        %[captured_data,time] = startbtn2.startForeground(); 
        disp('start at this moment'); 
        
        % This part is a prescan.
%         Data = startForeground(startbtn2); %Start the Session in Foreground;         
%         Data = diff(Data,1,1);
%         Data = [Data;0]; %Data; % Add a zero after diff.         
%         Data = reshape(Data,[sqrt(length(Data)),sqrt(length(Data))])'; 
%         %Data=reshape(Data,[Num_Pixel*Num_line_aver,Num_Line])'; 
%         gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Data);
%         colormap('hot'); colorbar; axis off;
        
        %Time series of the interferograms:
       % Here starts the z scan.
        % z_scan = linspace(-Scan_diameter_axial/2, Scan_diameter_axial/2, Num_Pixel_axial); %Axial scanning range;
        z_scan = linspace(-ZLength/2, ZLength/2, Num_Pixel_Z); % Axial scanning range;
        
        Image1 = zeros(Num_Line, Num_Pixel_X * Num_line_aver, Num_Pixel_Z); % 3D image data.
        Image2 = zeros(Num_Line, Num_Pixel_X * Num_line_aver, Num_Pixel_Z);
        handles.image1 = zeros(Num_Line, Num_Pixel_X * Num_line_aver, Num_Pixel_Z); % 3D image data.
        guidata(hObject, handles); % This is for saving the image.
        handles.image2 = zeros(Num_Line, Num_Pixel_X * Num_line_aver, Num_Pixel_Z); % 3D image data.
        guidata(hObject, handles); % This is for saving the image.
        
        for ii=1:Num_Pixel_Z
            msgstr = sprintf('Scaning frame %5.0f out of %5.0f frames...', ii, Num_Pixel_Z); 
            set(handles.text10,'string',msgstr); 
            set(handles.text10,'BackgroundColor','r'); 
            
            stop(s); release(s);    % This part is needed.
            
            % drawnow
            if get(handles.StopBtn, 'userdata') % Stop the loop if the stop buttton is pressed.
                break;
            end
            
%             handles.startbtn2 = startbtn2; % Transfer startbtn2 to the handle.
%             guidata(hObject, handles);
            
            z1 = z_scan(ii);
            outputSignal3 = z1*0.096*ones(size(outputSignal1)) + 4.6159; %Control axial position;4.8(in the beginning) 0,384  
            % Adjust the the two parameters so that we are imaging the same planes as in Imspector. 
            % outputSignal3=z1*0.0975*ones(size(outputSignal1))+5.484; %Control aixal position;4.8(in the beginning)  4.8975 
            % at the beginning.
            
            % queueOutputData(startbtn2,[outputSignal1 outputSignal2 outputSignal3 outputSignal4 outputSignal5]);
            queueOutputData(s,[outputSignal1 outputSignal2 outputSignal3 outputSignal4]);
            % OutputsL outputSignal,2,3 are analogy signals of scanning,
            % outputSignal4 is digital output for line trigger and
            % outputSignal5 is digital output for frame trigger.
            % Frame triggers can be unavailable if the number of lines in each
            % frame is exactly the same.

            %Clocked Counter Output(to synchronize line trigger): 
            % ch = startbtn2.Channels(5); % Here it becomes channel 5. 
%             ch.Frequency = 1/(Line_dwlltime*Num_line_aver); %Trigger output frequency; 
%             % ch.InitialDelay = Line_dwlltime/0.95; %Innitial delay; 
%             ch.DutyCycle = 1e-4;  %Duty cycle; 10ns Pulse width; 

            Data = startForeground(s); %Start the Session in Foreground;
            stop(s);  release(s);
            
            
            % Show the aquired image -- channel red
            Data1 = diff(Data(:,1),1,1);
            Data1 = [Data1; 0]; %Data; 
            Image1(:,:,ii) = reshape(Data1,[Num_Pixel_X*Num_line_aver, Num_Line])'; %Interferogram 1;        
            gcf; set(gcf,'currentaxes',handles.axes1); imagesc(Image1(:,:,ii)); 
            % caxis([0,50]); % Force the maximum intensity to be 50.
            colormap('hot'); colorbar; axis off;
            axis image

            handles.image1(:,:,ii) = Image1(:,:,ii); % Transfer Image1 to the handle.
            guidata(hObject, handles); % This is for saving the image.

            % Show the aquired image -- channel blue
            Data2 = diff(Data(:,2),1,1);
            Data2 = [Data2; 0]; %Data; 
            Image2(:,:,ii) = reshape(Data2,[Num_Pixel_X*Num_line_aver, Num_Line])'; %Interferogram 1;        
            gcf; set(gcf,'currentaxes',handles.axes2);
            imagesc(Image2(:,:,ii)); 
            % caxis([0,50]); % Force the maximum intensity to be 50.
            colormap('hot'); colorbar; axis off;
            axis image

            handles.image2(:,:,ii) = Image2(:,:,ii); % Transfer Image2 to the handle.
            guidata(hObject, handles);          
        end
        
        guidata(hObject, handles);          % May not be necessary.  
        
        %%=================== close all shutters =======================
        s = daq.createSession('ni'); %Create a Session;
        addDigitalChannel(s,'Dev2','Port0/Line1:3','OutputOnly'); % 3 for Confocal 640 nm, 2 for Tisa, 1 for 473.
        addDigitalChannel(s,'Dev2','Port0/Line5','OutputOnly'); % 5 is for 561 and 473 nm shutters.
        addDigitalChannel(s,'Dev2','Port0/Line7','OutputOnly'); % Start trigger of bh board
        outputSingleScan(s,[0 0 0 0 0]);
        stop(s);  release(s);
        %%========= end of shutter settings ========

        zout = daq.createSession('ni'); % create a session to apply voltage.
        addAnalogOutputChannel(zout,'Dev2',2,'Voltage'); % The channel that control the z position.
        outputSingleScan(zout,init_z); % Send the measured voltage before scanning starts to the z piezo.
        stop(zout);
        release(zout);
        
        %state declaration
        set(handles.text10,'string','Scanning finished!'); 
        set(handles.text10,'BackgroundColor','y'); 
        disp('finished!');
        
        %==================== Save the recorded images automatically.
        autosave_value = get(handles.AutoSave_2, 'Value');
        if(autosave_value)
            folder = get(handles.Path_2, 'String');
            name1 = 'scan_2_image_1.tif';
            name2 = 'scan_2_image_2.tif';
            full_path1 = strcat(folder, name1);
            full_path2 = strcat(folder, name2);
            options.message = false; % Show the message in the message box, not in the com window.
            saveastiff(uint16(Image1),full_path1,options);
            saveastiff(uint16(Image2),full_path2,options);
        end
        %----============Autosave finishes ---
        
    end

% --- Executes on button press in StartBtn12.
function StartBtn12_Callback(hObject, eventdata, handles)
% hObject    handle to StartBtn12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

StartBtn_Callback(hObject, eventdata, handles);
StartBtn2_Callback(hObject, eventdata, handles);


% --- Executes on button press in AutoSave_2.
function AutoSave_2_Callback(hObject, eventdata, handles)
% hObject    handle to AutoSave_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AutoSave_2


% --- Executes on button press in StartBH_2.
function StartBH_2_Callback(hObject, eventdata, handles)
% hObject    handle to StartBH_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of StartBH_2


% --- Executes on button press in AutoSave_1.
function AutoSave_1_Callback(hObject, eventdata, handles)
% hObject    handle to AutoSave_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AutoSave_1


% --- Executes on button press in StartBH_1.
function StartBH_1_Callback(hObject, eventdata, handles)
% hObject    handle to StartBH_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of StartBH_1



function Path_1_Callback(hObject, eventdata, handles)
% hObject    handle to Path_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Path_1 as text
%        str2double(get(hObject,'String')) returns contents of Path_1 as a double


% --- Executes during object creation, after setting all properties.
function Path_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Path_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Path_2_Callback(hObject, eventdata, handles)
% hObject    handle to Path_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Path_2 as text
%        str2double(get(hObject,'String')) returns contents of Path_2 as a double


% --- Executes during object creation, after setting all properties.
function Path_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Path_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in AveLine.
function AveLine_Callback(hObject, eventdata, handles)
% hObject    handle to AveLine (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AveLine


% --- Executes on button press in Copy_1.
function Copy_1_Callback(hObject, eventdata, handles)
% hObject    handle to Copy_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
XLength = get(handles.XLength, 'String'); % scan area.
set(handles.XLength_2, 'String', XLength);
Num_Pixel_X = str2double(get(handles.XPixNum, 'string'));
set(handles.XPixNum_2, 'String', Num_Pixel_X);
PxSizeX_1 = get(handles.PxSizeX_1, 'String');
set(handles.PxSizeX_2, 'String', PxSizeX_1);

% --- Executes on button press in Copy_2.
function Copy_2_Callback(hObject, eventdata, handles)
% hObject    handle to Copy_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
YLength = get(handles.YLength, 'String'); % scan area.
set(handles.YLength_2, 'String', YLength);
Num_Pixel_Y = str2double(get(handles.YPixNum, 'string'));
set(handles.YPixNum_2, 'String', Num_Pixel_Y);
PxSizeY_1 = get(handles.PxSizeY_1, 'String');
set(handles.PxSizeY_2, 'String', PxSizeY_1);


% --- Executes on button press in Copy_3.
function Copy_3_Callback(hObject, eventdata, handles)
% hObject    handle to Copy_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ZLength = get(handles.ZLength, 'String'); % scan area.
set(handles.ZLength_2, 'String', ZLength);
Num_Pixel_Z = str2double(get(handles.ZPixNum, 'string'));
set(handles.ZPixNum_2, 'String', Num_Pixel_Z);
PxSizeZ_1 = get(handles.PxSizeZ_1, 'String');
set(handles.PxSizeZ_2, 'String', PxSizeZ_1);


% --- Executes on button press in Copy_4.
function Copy_4_Callback(hObject, eventdata, handles)
% hObject    handle to Copy_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
PixTime = get(handles.PixTime, 'String');
set(handles.PixTime_2, 'String', PixTime);
Line_aver = get(handles.AveLine, 'Value');  %0 for averaging, and 1 for not doing averaging.
set(handles.AveLine_2, 'Value', Line_aver);
Num_line_aver = get(handles.NumAveLine,'string'); % Number of lines to average
set(handles.NumAveLine_2, 'String', Num_line_aver);

% --- Executes on button press in Copy_5.
function Copy_5_Callback(hObject, eventdata, handles)
% hObject    handle to Copy_5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Path_1 = get(handles.Path_1, 'String');
set(handles.Path_2, 'String', Path_1);



function PxSizeX_1_Callback(hObject, eventdata, handles)
% hObject    handle to PxSizeX_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PxSizeX_1 as text
%        str2double(get(hObject,'String')) returns contents of PxSizeX_1 as a double

Objective = get(handles.Objective, 'SelectedObject');
Selected_Obj = Objective.String;
len = str2double(get(handles.XLength, 'String'));
px_size = str2double(get(handles.PxSizeX_1, 'String'));

switch Selected_Obj
    case '100x'
        px_num = len*1000*0.605/px_size;
    case '63x'
        px_num = len*1000/px_size;
end

set(handles.XPixNum, 'String', round(px_num));


% --- Executes during object creation, after setting all properties.
function PxSizeX_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PxSizeX_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PxSizeY_1_Callback(hObject, eventdata, handles)
% hObject    handle to PxSizeY_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PxSizeY_1 as text
%        str2double(get(hObject,'String')) returns contents of PxSizeY_1 as a double

Objective = get(handles.Objective, 'SelectedObject');
Selected_Obj = Objective.String;
len = str2double(get(handles.YLength, 'String'));
px_size = str2double(get(handles.PxSizeY_1, 'String'));

switch Selected_Obj
    case '100x'
        px_num = len*1000*0.605/px_size;
    case '63x'
        px_num = len*1000/px_size;
end

set(handles.YPixNum, 'String', round(px_num));


% --- Executes during object creation, after setting all properties.
function PxSizeY_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PxSizeY_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PxSizeZ_1_Callback(hObject, eventdata, handles)
% hObject    handle to PxSizeZ_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PxSizeZ_1 as text
%        str2double(get(hObject,'String')) returns contents of PxSizeZ_1 as a double

len = str2double(get(handles.ZLength, 'String'));
px_size = str2double(get(handles.PxSizeZ_1, 'String'));
px_num = len*1000/px_size;
set(handles.ZPixNum, 'String', round(px_num));


% --- Executes during object creation, after setting all properties.
function PxSizeZ_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PxSizeZ_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PxSizeX_2_Callback(hObject, eventdata, handles)
% hObject    handle to PxSizeX_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PxSizeX_2 as text
%        str2double(get(hObject,'String')) returns contents of PxSizeX_2 as a double
Objective = get(handles.Objective, 'SelectedObject');
Selected_Obj = Objective.String;
len = str2double(get(handles.XLength_2, 'String'));
px_size = str2double(get(handles.PxSizeX_2, 'String'));

switch Selected_Obj
    case '100x'
        px_num = len*1000*0.605/px_size;
    case '63x'
        px_num = len*1000/px_size;
end

set(handles.XPixNum_2, 'String', round(px_num));


% --- Executes during object creation, after setting all properties.
function PxSizeX_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PxSizeX_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function PxSizeY_2_Callback(hObject, eventdata, handles)
% hObject    handle to PxSizeY_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PxSizeY_2 as text
%        str2double(get(hObject,'String')) returns contents of PxSizeY_2 as a double

Objective = get(handles.Objective, 'SelectedObject');
Selected_Obj = Objective.String;
len = str2double(get(handles.YLength_2, 'String'));
px_size = str2double(get(handles.PxSizeY_2, 'String'));

switch Selected_Obj
    case '100x'
        px_num = len*1000*0.605/px_size;
    case '63x'
        px_num = len*1000/px_size;
end

set(handles.YPixNum_2, 'String', round(px_num));


% --- Executes during object creation, after setting all properties.
function PxSizeY_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PxSizeY_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PxSizeZ_2_Callback(hObject, eventdata, handles)
% hObject    handle to PxSizeZ_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PxSizeZ_2 as text
%        str2double(get(hObject,'String')) returns contents of PxSizeZ_2 as a double
len = str2double(get(handles.ZLength_2, 'String'));
px_size = str2double(get(handles.PxSizeZ_2, 'String'));
px_num = len*1000/px_size;
set(handles.ZPixNum_2, 'String', round(px_num));


% --- Executes during object creation, after setting all properties.
function PxSizeZ_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PxSizeZ_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected object is changed in Objective.
function Objective_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in Objective 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% X, Y pixel numbers change when the selection is changed.
PxSizeX_1_Callback(hObject, eventdata, handles);
PxSizeY_1_Callback(hObject, eventdata, handles);
PxSizeX_2_Callback(hObject, eventdata, handles);
PxSizeY_2_Callback(hObject, eventdata, handles);

